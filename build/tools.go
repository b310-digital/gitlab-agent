//go:build tools

package build

import (
	_ "github.com/alecthomas/jsonschema" // required by protoc-gen-jsonschema
	_ "github.com/chrusty/protoc-gen-jsonschema"
	_ "github.com/envoyproxy/protoc-gen-validate"
	_ "github.com/sirupsen/logrus"      // required by protoc-gen-jsonschema
	_ "github.com/xeipuuv/gojsonschema" // required by protoc-gen-jsonschema
	_ "google.golang.org/grpc/cmd/protoc-gen-go-grpc"
)
