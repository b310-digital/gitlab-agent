# coding: utf-8
require_relative './lib/version.rb'

Gem::Specification.new do |spec|
  spec.name          = 'gitlab-kas-grpc'
  spec.version       = Gitlab::Agent::VERSION
  spec.homepage      = 'https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent'

  spec.summary       = 'Auto-generated gRPC client for KAS'

  spec.authors       = ['Tiger Watson', 'Timo Furrer', 'Taka Nishida']
  spec.email         = ['twatson@gitlab.com', 'tfurrer@gitlab.com', 'tnishida@gitlab.com']
  spec.license       = 'MIT'

  spec.metadata = {
    "homepage_uri"      => "https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent",
    "bug_tracker_uri"   => "https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues",
    "source_code_uri"   => "https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/tree/master",
    "documentation_uri" => "https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/tree/master/doc",
  }

  spec.files         = Dir['lib/**/*.rb']
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'grpc', '~> 1.0'
  spec.required_ruby_version = '>=3.2'
end
