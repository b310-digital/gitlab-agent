package kascfg

func (x *GitLabCF) ExternalURL() string {
	if x.ExternalUrl != nil {
		return *x.ExternalUrl
	}
	return x.Address
}
