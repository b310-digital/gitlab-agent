package agentkapp

import (
	"context"
	"fmt"
	"log/slog"
	"net"
	"time"

	"github.com/ash2k/stager"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/validator"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/ioz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/nettool"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
)

const (
	apiListenGracePeriod      = time.Second
	apiListenMaxConnectionAge = 2 * time.Hour
)

// listenAPIServer represents agentk API that kas can talk to.
// It can be either:
// - in-memory. This is the case when agentk connects to kas.
// - exposed on an address. This is the case when kas connects to agentk.
type listenAPIServer struct {
	log                          *slog.Logger
	listenNetwork, listenAddress string
	certFile, keyFile            string

	auxCancel context.CancelFunc
	server    *grpc.Server
}

func newListenAPIServer(log *slog.Logger, ot *obsTools, factory modshared.RPCAPIFactory,
	listenNetwork, listenAddress, certFile, keyFile,
	jwtSecretFile, jwtIssuer, jwtAudience string) (*listenAPIServer, error) {

	credsOpt, err := grpctool.MaybeTLSCreds(certFile, keyFile)
	if err != nil {
		return nil, err
	}

	jwtSecret, err := ioz.LoadBase64Secret(jwtSecretFile)
	if err != nil {
		return nil, fmt.Errorf("API auth secret file: %w", err)
	}

	jwtAuther := grpctool.NewEdDSAJWTAuther(jwtSecret, jwtIssuer, jwtAudience, func(ctx context.Context) *slog.Logger {
		return modshared.RPCAPIFromContext(ctx).Log()
	})

	auxCtx, auxCancel := context.WithCancel(context.Background())
	keepaliveOpt, sh := grpctool.MaxConnectionAge2GRPCKeepalive(auxCtx, apiListenMaxConnectionAge)
	opts := []grpc.ServerOption{
		keepaliveOpt,
		grpc.StatsHandler(otelgrpc.NewServerHandler(
			otelgrpc.WithTracerProvider(ot.tp),
			otelgrpc.WithMeterProvider(ot.mp),
			otelgrpc.WithPropagators(ot.p),
			otelgrpc.WithMessageEvents(otelgrpc.ReceivedEvents, otelgrpc.SentEvents),
		)),
		grpc.StatsHandler(sh),
		grpc.SharedWriteBuffer(true),
		grpc.ChainStreamInterceptor(
			ot.streamProm, // 1. measure all invocations
			modshared.StreamRPCAPIInterceptor(factory), // 2. inject RPC API
			jwtAuther.StreamServerInterceptor,          // 3. auth and maybe log
			grpc_validator.StreamServerInterceptor(),   // x. wrap with validator
		),
		grpc.ChainUnaryInterceptor(
			ot.unaryProm, // 1. measure all invocations
			modshared.UnaryRPCAPIInterceptor(factory), // 2. inject RPC API
			jwtAuther.UnaryServerInterceptor,          // 3. auth and maybe log
			grpc_validator.UnaryServerInterceptor(),   // x. wrap with validator
		),
		grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{
			MinTime:             20 * time.Second,
			PermitWithoutStream: true,
		}),
	}
	opts = append(opts, credsOpt...)
	return &listenAPIServer{
		log:           log,
		listenNetwork: listenNetwork,
		listenAddress: listenAddress,
		certFile:      certFile,
		keyFile:       keyFile,
		server:        grpc.NewServer(opts...),
		auxCancel:     auxCancel,
	}, nil
}

func (s *listenAPIServer) Start(stage stager.Stage) {
	grpctool.StartServer(stage, s.server,
		func() (net.Listener, error) {
			lis, err := nettool.ListenWithOSTCPKeepAlive(s.listenNetwork, s.listenAddress)
			if err != nil {
				return nil, err
			}
			addr := lis.Addr()
			s.log.Info("API endpoint is up",
				logz.NetNetworkFromAddr(addr),
				logz.NetAddressFromAddr(addr),
			)
			return lis, nil
		},
		func() {
			time.Sleep(apiListenGracePeriod)
			// We first want gRPC server to send GOAWAY and only then return from the RPC handlers.
			// So we delay signaling the handlers.
			// See https://github.com/grpc/grpc-go/issues/6830 for more background.
			// Start a goroutine in a second and...
			time.AfterFunc(time.Second, func() {
				s.auxCancel() // ... signal running RPC handlers to stop.
			})
		},
		func() {},
	)
}
