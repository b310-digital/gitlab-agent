package agentkapp

import (
	"context"
	"io"
	"log/slog"
	"os"
	"sync"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/syncz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/agentcfg"
)

func (a *App) logHandler(envVar string, levelEnum agentcfg.LogLevelEnum, writer io.Writer) (slog.Handler, *slog.LevelVar, error) {
	l := os.Getenv(envVar)
	if l == "" {
		l = levelEnum.String()
	}
	levelVar := &slog.LevelVar{}
	err := levelVar.UnmarshalText([]byte(l))
	if err != nil {
		return nil, nil, err
	}

	handler := &agentIDHandler{
		delegate: slog.NewJSONHandler(writer, &slog.HandlerOptions{
			Level: levelVar,
		}),
		agentID: a.AgentID,
	}
	return handler, levelVar, nil
}

// agentIDHandler wraps a slog.Handler to add agent id field if agent id is available.
type agentIDHandler struct {
	mu       sync.Mutex
	delegate slog.Handler
	agentID  *syncz.ValueHolder[int64] // if nil, agent id has been added to the delegate.
}

func (h *agentIDHandler) Enabled(ctx context.Context, level slog.Level) bool {
	h.mu.Lock()
	d := h.delegate
	h.mu.Unlock()
	return d.Enabled(ctx, level)
}

func (h *agentIDHandler) WithAttrs(attrs []slog.Attr) slog.Handler {
	d, agentID := h.tryToGetAgentID()
	d = d.WithAttrs(attrs)
	if agentID == nil { // d already has agent id attr
		return d
	}
	return &agentIDHandler{
		delegate: d,
		agentID:  h.agentID,
	}
}

func (h *agentIDHandler) WithGroup(name string) slog.Handler {
	d, agentID := h.tryToGetAgentID()
	d = d.WithGroup(name)
	if agentID == nil { // d already has agent id attr
		return d
	}
	return &agentIDHandler{
		delegate: d,
		agentID:  h.agentID,
	}
}

func (h *agentIDHandler) Handle(ctx context.Context, r slog.Record) error {
	d, _ := h.tryToGetAgentID()
	return d.Handle(ctx, r)
}

func (h *agentIDHandler) tryToGetAgentID() (slog.Handler, *syncz.ValueHolder[int64]) {
	h.mu.Lock()
	defer h.mu.Unlock()
	if h.agentID == nil {
		return h.delegate, nil
	}
	agentID, ok := h.agentID.TryGet()
	if ok {
		h.delegate = h.delegate.WithAttrs([]slog.Attr{logz.AgentID(agentID)})
		h.agentID = nil
	}
	return h.delegate, h.agentID
}
