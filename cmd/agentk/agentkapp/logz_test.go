package agentkapp

import (
	"bytes"
	"context"
	"log/slog"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/syncz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
)

func TestAgentIDHandler(t *testing.T) {
	b := &bytes.Buffer{}
	agentID := syncz.NewValueHolder[int64]()
	h := &agentIDHandler{
		delegate: slog.NewTextHandler(b, &slog.HandlerOptions{}),
		agentID:  agentID,
	}
	r := slog.NewRecord(time.Time{}, slog.LevelInfo, "test", 0)
	err := h.Handle(context.Background(), r)
	require.NoError(t, err)

	err = agentID.Set(testhelpers.AgentID)
	require.NoError(t, err)

	err = h.Handle(context.Background(), r)
	require.NoError(t, err)

	expected := "level=INFO msg=test\nlevel=INFO msg=test agent_id=123\n"

	assert.Equal(t, expected, b.String())
}
