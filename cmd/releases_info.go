package cmd

import (
	_ "embed"
	"strings"
)

var (
	// gitlabReleasesInfo contains a raw comma separated list of GitLab releases in the format of `Major.Minor`
	// without any spacing.
	//go:embed vendored/releases_info.txt
	gitlabReleasesInfo string

	// GitLabReleasesList contains a list of GitLab Releases in the format of `Major.Minor`,
	// for example `17.10` or `16.5`.
	GitLabReleasesList []string
)

func init() {
	GitLabReleasesList = strings.Split(gitlabReleasesInfo, ",")
}
