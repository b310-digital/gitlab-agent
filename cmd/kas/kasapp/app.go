package kasapp

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"os/exec"
	"strings"

	"github.com/bufbuild/protovalidate-go"
	"github.com/go-logr/logr"
	"github.com/spf13/cobra"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/metric"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/kascfg"
	"go.opentelemetry.io/otel"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"
	"k8s.io/klog/v2"
	"sigs.k8s.io/yaml"
)

type App struct {
	ConfigurationFile string
}

func (a *App) Run(ctx context.Context) (retErr error) {
	cfg, err := LoadConfigurationFile(ctx, a.ConfigurationFile)
	if err != nil {
		return err
	}
	ApplyDefaultsToKASConfigurationFile(cfg)
	v, err := protovalidate.New()
	if err != nil {
		return err
	}
	err = v.Validate(cfg)
	if err != nil {
		return fmt.Errorf("config validate: %w", err)
	}
	log, grpcLog, err := loggerFromConfig(cfg.Observability.Logging)
	if err != nil {
		return err
	}
	grpclog.SetLoggerV2(grpcLog) // pipe gRPC logs into slog
	logrLogger := logr.FromSlogHandler(log.Handler())
	// Kubernetes uses klog so here we pipe all logs from it to our logger via an adapter.
	klog.SetLogger(logrLogger)
	otel.SetLogger(logrLogger)
	otel.SetErrorHandler((*metric.OtelErrorHandler)(log))
	app := ConfiguredApp{
		Log:           log,
		Validator:     v,
		Configuration: cfg,
	}
	return app.Run(ctx)
}

// LoadConfigurationFile loads the configuration file.
// This function does not run any validation logic.
func LoadConfigurationFile(ctx context.Context, configFile string) (*kascfg.ConfigurationFile, error) {
	configYAML, err := os.ReadFile(configFile) //nolint: gosec
	if err != nil {
		return nil, fmt.Errorf("configuration file: %w", err)
	}
	cfg, err := yamlToConfig(configYAML)
	if err != nil {
		return nil, err
	}
	gen := cfg.GetConfig().GetCommand()
	if gen != "" {
		genCfg, genErr := runConfigCommand(ctx, cfg.Config)
		if genErr != nil {
			return nil, fmt.Errorf("config command: %w", genErr)
		}
		proto.Merge(cfg, genCfg)
	}
	return cfg, nil
}

func NewCommand() *cobra.Command {
	a := App{}
	c := &cobra.Command{
		Use:   "kas",
		Short: "GitLab Kubernetes Agent Server",
		Args:  cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			return a.Run(cmd.Context())
		},
		SilenceErrors: true,
		SilenceUsage:  true,
	}
	c.Flags().StringVar(&a.ConfigurationFile, "configuration-file", "", "Configuration file to use (YAML)")
	cobra.CheckErr(c.MarkFlagRequired("configuration-file"))

	return c
}

func loggerFromConfig(loggingCfg *kascfg.LoggingCF) (*slog.Logger, *grpctool.Logger, error) {
	var level, grpcLevel slog.Level
	err := level.UnmarshalText([]byte(loggingCfg.Level.String()))
	if err != nil {
		return nil, nil, err
	}
	err = grpcLevel.UnmarshalText([]byte(loggingCfg.GrpcLevel.String()))
	if err != nil {
		return nil, nil, err
	}
	lockedWriter := &logz.LockedWriter{
		Writer: os.Stderr,
	}
	log := slog.New(slog.NewJSONHandler(lockedWriter, &slog.HandlerOptions{
		Level: level,
	}))
	grpcLog := &grpctool.Logger{
		Handler: slog.NewJSONHandler(lockedWriter, &slog.HandlerOptions{
			Level: grpcLevel,
		}),
	}

	return log, grpcLog, nil
}

func runConfigCommand(ctx context.Context, cf *kascfg.ConfigCF) (*kascfg.ConfigurationFile, error) {
	cmd, args := splitCommand(cf.Command)
	genCmd := exec.CommandContext(ctx, cmd, args...) //nolint: gosec
	genCmd.Stderr = os.Stderr                        // pipe cmd's stderr into our own stderr
	genConfigYAML, err := genCmd.Output()
	if err != nil {
		return nil, err
	}
	return yamlToConfig(genConfigYAML)
}

func yamlToConfig(configYAML []byte) (*kascfg.ConfigurationFile, error) {
	configJSON, err := yaml.YAMLToJSON(configYAML)
	if err != nil {
		return nil, fmt.Errorf("YAMLToJSON: %w", err)
	}
	cfg := &kascfg.ConfigurationFile{}
	err = protojson.Unmarshal(configJSON, cfg)
	if err != nil {
		return nil, fmt.Errorf("protojson.Unmarshal: %w", err)
	}
	return cfg, nil
}

func splitCommand(cmd string) (string, []string) {
	cmdAndArgs := strings.Split(cmd, " ")
	return cmdAndArgs[0], cmdAndArgs[1:]
}
