## 17.2.1 (2024-07-24)

No changes.

## 17.2.0 (2024-07-17)

### Added (4 changes)

- [Support after stop callback for gRPC servers](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/5fc3b895910eb6aedc891336127d233de01926a7) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1591))
- [New agentk authn header](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/516eae0f4b384a14671f42d7aa590a5351a17b20) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1592))
- [Support username for Redis Sentinel](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/221901f825928c5c2e8df1ece340ed10b4e92466) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1571))
- [Support kas->agentk connections](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/d11dc332993baca56eec87e224ba3771eb431ff2) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1547))

### Fixed (2 changes)

- [Fixed shutdown errors](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/95e167e36f2c02adb480fed615bc8ec938c7e551) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1601))
- [Fix unsynchronized access](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/ee108ff4fe7c157ac52106b9a53ce45621747688) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1579))

### Changed (7 changes)

- [Move ValueHolder](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/e18574f89a239162c4fbb78e61e7569ef9da42eb) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1606))
- [Bump dependencies](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/9073117f87266efdec8d83cc44f31e72b8e8867b) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1600))
- [zap -> slog for logging](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/a0865860aa8f86dafecff9c0c6c7f7a7aa736c26) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1593))
- [Re-vendored Gitaly client and proto files](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/6fbbfe0a986da93ef8de6850abf40c7d5dcd1d5f) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1574))
- [Regenerate protobuf](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/6fb8c0e5cf96e3f4ba6bdad2e3e9aae300014a87) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1572))
- [Bump dependencies](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/ea854c5f2e7c3e5abc90b861fe06a87278b7516b) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1572))
- [Refactor to use rand v2 API](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/b97b26bf7ec5780b76974544bb9729a00f93b4ed) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1502))

### Removed (2 changes)

- [Remove striping from registry](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/d4f7b2ef67d949fbb382c292f967ff4d3f991039) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1591))
- [Remove unused visitors](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/94a5bff23ffc99e143efb86a24eb0202a8429f0c) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1574))

### Performance (1 change)

- [Asynchronous IO in tunnel registry](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/15d0814e884f80b4db2489ac136ee6c853b553f9) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1591))

### Other (1 change)

- [Upgrade to Go 1.22](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/023d20cfa54092c48180385c84a7f7d8113f97e0) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1502))

## 17.1.3 (2024-07-24)

No changes.

## 17.1.2 (2024-07-09)

No changes.

## 17.1.1 (2024-06-25)

### Fixed (1 change)

- [Fix unsynchronized access](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/7ba214f2738d5ba70dca7f6a6e2c0b5427615edb)

## 17.1.0 (2024-06-19)

### Added (2 changes)

- [Support setting log levels in agentk via env vars](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/f9f51d85ccd6df9682f525ff2ec3834a0a3b3abb) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1560))
- [Implement intentional unregistration of an agent](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/a18d2648cd5fda0fe8c1b9d53a52c0a32c82644e) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1550))

### Fixed (4 changes)

- [Fix concurrent access to local agent expiring hash data for GC](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/57dfe7006c88439f8b34b6234cdd3fe0ec8edbc1) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1541))
- [Use separate context for resolving agent infos during error handling](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/639a34115f1bf3c0b60268878c425507cb335d39) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1536))
- [Fix the metric name mix up for routing tunnel timeouts](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/411e5e9329344194460b89d2760a713f710a6b1f) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1525))
- [Fix build date parsing for CentOS 7-based distros](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/b988bbaffb0dc564fb63b33c3bc83aeea2d5830d) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1507))

### Changed (11 changes)

- [Bump Go version](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/63d6258d9ab77defb2a45a8a2bd39be3417c277b) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1566))
- [Update golangci-lint](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/11f8e3335507b672a07c80d40d46d558fc8af26c) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1566))
- [Move profiler enabled check into factory](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/5991fe7230cd6e94355dff5ac0aa096e4f28b8bf) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1566))
- [Bump protoc-gen-go-grpc](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/712810530c5e8fe8d3cddc5bc05cb35aa11458de) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1555))
- [Bump dependencies](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/6690cf74c5a2a2ac32fa4038ef05af4086d9269c) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1555))
- [Go 1.22.4](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/4fd57c761d73e3693d3535f076b4adcacd4995a8) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1555))
- [Move stream preparation into plugin](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/958cb54cd8b5afc731f8f29fb8b5d2f97bd6a20b) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1553))
- [Remove temporary GetConfiguration logs](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/5341aeeb414ec0cf4c0fde7d513c22250ae49987) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1549))
- [Introduce KAS shutdown logs](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/b3cbed00f4cdb0c3d2a9184e0f75f59b7cd0fda4) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1538))
- [Use fields instead of dynamic error message](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/fa0e1477d1dd1ddec26918bfe6517be40673a8f7) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1532))
- [Allow empty msg when reporting an error](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/87eca357700488aaff03b0a060a93e48d2534961) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1532))

### Performance (2 changes)

- [Do not tunnel reserved headers](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/7452592c123eafcc99faa144e17094e934cd1cc7) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1553))
- [Route requests in a ClientConn, remove internal server](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/commit/7ace2a2295d97d83dfff99eabb35eb78af359e39) ([merge request](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1499))

## 17.0.5 (2024-07-24)

No changes.

## 17.0.4 (2024-07-09)

No changes.

## 17.0.3 (2024-06-25)

### Fixed (1 change)

- [Fix concurrent access to local agent expiring hash data for GC](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/2f01679417f9a33c1de4aca9903651561b175ae8)

## 17.0.2 (2024-06-11)

### Fixed (1 change)

- [17.0 backport: Allow text/plain Content-Type in proxy](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/0269509036a455e8aa083c77885acfde2a3d4a29)

### Security (1 change)

- [Ensure required AgentMeta is present](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/28db01e9a414e797daec0cd4c4821376654e5003) ([merge request](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/merge_requests/10))

## 17.0.1 (2024-05-21)

### Fixed (1 change)

- [Fix build date parsing for CentOS 7-based distros](gitlab-org/security/cluster-integration/gitlab-agent@0642f68cd0b7a9046aec97daec722f52b4b209be)

## 17.0.0 (2024-05-15)

### Added (2 changes)

- [Generate Ruby bindings for Server Info RPC](gitlab-org/cluster-integration/gitlab-agent@a4e72b4703a164534aabca74f4b81662276969b5) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1455))
- [Implement Server Info RPC interface](gitlab-org/cluster-integration/gitlab-agent@1f08c0d278f685b59b44231ac2ed5856f4fa7305) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1455))

### Fixed (2 changes)

- [Do not allow to inject version from outside](gitlab-org/cluster-integration/gitlab-agent@1dd0ece91d724bc090c3e3b88433883b906bd96d) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1470))
- [Use -u instead of --utc for date command](gitlab-org/cluster-integration/gitlab-agent@b18d2a66a3280f12ff6049b0678360bf45ca0ace) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1469))

### Changed (4 changes)

- [Drop sigs.k8s.io/controller-runtime dependency](gitlab-org/cluster-integration/gitlab-agent@298927f4512163308de680121eb7cd2937db7223) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1500))
- [Bump Go dependencies](gitlab-org/cluster-integration/gitlab-agent@59f397dde4a8fdd08ede5dc8b40e58686a97df0b) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1500))
- [Use standard datetime format for build time info](gitlab-org/cluster-integration/gitlab-agent@b429f282f0e5ff65686c865814abb1c1eb060b00) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1466))
- [Increase max size of Agent Meta Version string](gitlab-org/cluster-integration/gitlab-agent@203e537e74e1e0bec8ac3231e2a1eb6b607fbeec) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1457))

### Removed (3 changes)

- [Remove GetConnectedAgents gRPC service](gitlab-org/cluster-integration/gitlab-agent@2f349b55ad3b1473d11a477f49ea61e72e584b91) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1433))
- [Remove deprecated gRPC services](gitlab-org/cluster-integration/gitlab-agent@deaaa02c20861140917c184b30ec10fcf1ae3c8c) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1433))
- [Remove `ca-cert-file` CLI option in agentk](gitlab-org/cluster-integration/gitlab-agent@c5ee86f8c38171f20a6347eabff358361b85e657) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1431))

### Other (1 change)

- [Introduce support for VERSION file](gitlab-org/cluster-integration/gitlab-agent@19674f3d5a629086abafa2ec93922c2818ef8139) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1434))

## 16.11.7 (2024-07-23)

No changes.

## 16.11.6 (2024-07-09)

No changes.

## 16.11.5 (2024-06-25)

No changes.

## 16.11.4 (2024-06-11)

### Security (1 change)

- [Ensure required AgentMeta is present or handled when missing](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/0fd9a7d1576246b7e5eb97ec4461c23dafe37051) ([merge request](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/merge_requests/11))

## 16.11.3 (2024-05-21)

No changes.

## 16.11.2 (2024-05-07)

### Fixed (1 change)

- [Do not allow to inject version from outside](gitlab-org/security/cluster-integration/gitlab-agent@141707b32694692d495979eb3191004de3964962)

### Changed (1 change)

- [Increase max size of Agent Meta Version string](gitlab-org/security/cluster-integration/gitlab-agent@ebe741712fd4b178668c3485e1bd99cc041a14f9)

### Other (1 change)

- [Introduce support for VERSION file](gitlab-org/security/cluster-integration/gitlab-agent@aefc4f5a3513fb0d67283ec3d11649a601977d5d)

## 16.11.1 (2024-04-24)

No changes.

## 16.11.0 (2024-04-17)

### Added (1 change)

- [Opt-out option for Flux receiver creation](gitlab-org/cluster-integration/gitlab-agent@81485a3c96ec78b9fb1d96b053669fb467b6fa65)

### Changed (4 changes)

- [Tune Redis COUNT modifier for HSCAN](gitlab-org/cluster-integration/gitlab-agent@2cda802f2e292751aab4ed169ff6961b9281ac54)
- [Only reconcile GitRepository and Receiver when spec changes](gitlab-org/cluster-integration/gitlab-agent@389c01c15c21c1cb1801a5a121d696fc9d8a2890)
- [ScanAndGC -> independent Scan + GC](gitlab-org/cluster-integration/gitlab-agent@49938bd26b220dadd4da9644eb278e01e8b0fe23)
- [Update dependency trivy-k8s-wrapper to v0.2.15](gitlab-org/cluster-integration/gitlab-agent@acce190f70c52c701f1d64248305af47ed1023b3)

### Fixed (4 changes)

- [Stop erroring out when OWN_PRIVATE_API_HOST is not set](gitlab-org/cluster-integration/gitlab-agent@fad847403ffe99cf1cd7df0d6656a40a1737d2e0)
- [Fix garbage collection for keys registered with SetEX](gitlab-org/cluster-integration/gitlab-agent@3fd1c3df686fc07db7fba0ab98c3bf6a7490703b)
- [Fix KAS shutdown issue](gitlab-org/cluster-integration/gitlab-agent@9ace31f290dbef83724abb48d3665cf80f41a083)
- [Don't panic if module exits when config channel is closed](gitlab-org/cluster-integration/gitlab-agent@04d8e67a63c900e68e6aa5bd0f162d20ac3bc643)

## 16.10.9 (2024-07-23)

No changes.

## 16.10.8 (2024-06-25)

No changes.

## 16.10.7 (2024-06-11)

### Security (1 change)

- [Ensure required AgentMeta is present or handled when missing](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/574f447efc7eca56d73991ae0609809cedd99efd) ([merge request](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/merge_requests/12))

## 16.10.6 (2024-05-21)

No changes.

## 16.10.5 (2024-05-07)

### Fixed (1 change)

- [Do not allow to inject version from outside](gitlab-org/security/cluster-integration/gitlab-agent@cdcf385865c6fb2603f5f4e35cb1199315609274)

### Changed (1 change)

- [Increase max size of Agent Meta Version string](gitlab-org/security/cluster-integration/gitlab-agent@516f2f3232573e8d1eedf2e66e21258a53ba1f2a)

### Other (1 change)

- [Introduce support for VERSION file](gitlab-org/security/cluster-integration/gitlab-agent@0b1112a816876ac08e7161084749aadf4cdf88c7)

## 16.10.4 (2024-04-24)

No changes.

## 16.9.10 (2024-07-23)

No changes.

## 16.9.8 (2024-05-09)

No changes.

## 16.9.7 (2024-05-07)

### Fixed (1 change)

- [Do not allow to inject version from outside](gitlab-org/security/cluster-integration/gitlab-agent@ccfbedc0b0509fe37250042744d578bc3491325a)

### Changed (1 change)

- [Increase max size of Agent Meta Version string](gitlab-org/security/cluster-integration/gitlab-agent@37a22cc5e72f82f5168314fd62a6734a232ede9c)

### Other (1 change)

- [Introduce support for VERSION file](gitlab-org/security/cluster-integration/gitlab-agent@de9d1e4ea520890b8531335dd006c8c7e3c9c144)

## 16.9.6 (2024-04-24)

No changes.
