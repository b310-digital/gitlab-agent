package engine

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow/flow"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/errz"
	"go.starlark.net/starlark"
	"go.temporal.io/sdk/activity"
)

// activityState holds shared state that is used by all Temporal activities.
type activityState struct {
	source flow.Source
	httpRT http.RoundTripper
}

type RepoInfoCacheKey struct {
	// TODO identity must be part of the cache key
	ProjectID string
}

func (a *activityState) LoadAllEventHandlersActivity(ctx context.Context, input *LoadAllEventHandlersActivityInput) (*LoadAllEventHandlersActivityOutput, error) {
	recSrc := newRecordingSource(a.source)
	log := activity.GetLogger(ctx)
	runtime, err := flow.NewRuntimeFromSource(ctx, flow.RuntimeOptions{
		Source:      recSrc,
		ProjectPath: input.FlowProject.FullPath,
		Entrypoint:  input.Entrypoint,
		PrintFunc: func(t *starlark.Thread, msg string) {
			log.Info(msg)
		},
		NowFunc: time.Now,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to load flow: %w", err)
	}

	eventDict, err := eventToDict(input.Event)
	if err != nil {
		return nil, err
	}
	// FIXME: consider eventDict to be created if needed - that is, inject a callback func
	eh, err := runtime.EvaluateEventHandlerIds(input.Event.Type, eventDict)
	if err != nil {
		return nil, err
	}

	handlers := make([]*HandlerInfo, 0, len(eh))
	for _, h := range eh {
		handlers = append(handlers, &HandlerInfo{WorkflowId: h})
	}

	return &LoadAllEventHandlersActivityOutput{
		Handlers: handlers,
		Modules:  recSrc.modules,
	}, nil
}

func (a *activityState) HTTPDoActivity(ctx context.Context, input *HttpDoActivityInput) (output *HttpDoActivityOutput, retErr error) {
	var body io.Reader
	if len(input.Body) > 0 {
		body = bytes.NewReader(input.Body)
	}
	// TODO protect against server-side request forgery.
	req, err := http.NewRequestWithContext(ctx, input.Method, input.Url, body)
	if err != nil {
		return nil, err
	}
	req.Header = valuesMapToHTTPHeader(input.Header)
	resp, err := a.httpRT.RoundTrip(req) //nolint:bodyclose
	if err != nil {
		return nil, err
	}
	defer errz.SafeClose(resp.Body, &retErr)
	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return &HttpDoActivityOutput{
		StatusCode: uint32(resp.StatusCode),
		Status:     resp.Status,
		Header:     httpHeaderToValuesMap(resp.Header),
		Body:       data,
	}, nil
}
