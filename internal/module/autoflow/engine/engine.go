package engine

import (
	"context"
	"log/slog"
	"net/http"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitaly"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/cache"
	"go.starlark.net/starlark"
	tclient "go.temporal.io/sdk/client"
	tworker "go.temporal.io/sdk/worker"
)

const (
	defaultEntrypoint = ".gitlab/autoflow/main.star"
	varGitLabURL      = "gitlab_url"
	maxFileSize       = 100 * 1024
)

type Engine struct {
	log       *slog.Logger
	tc        tclient.Client
	taskQueue string
	tw        tworker.Worker
	ws        *workflowState
}

type Options struct {
	Log           *slog.Logger
	TC            tclient.Client
	GitalyPool    gitaly.PoolInterface
	GitLabClient  gitlab.ClientInterface
	RepoInfoCache *cache.CacheWithErr[RepoInfoCacheKey, *api.ProjectInfo]
	HTTPRT        http.RoundTripper
	TaskQueue     string
	// Must not have / at the end.
	GitLabURL string
}

func NewEngine(opts Options) (*Engine, error) {
	tw := tworker.New(opts.TC, opts.TaskQueue, tworker.Options{
		//MaxConcurrentActivityExecutionSize:      0,
		//WorkerActivitiesPerSecond:               0,
		//MaxConcurrentLocalActivityExecutionSize: 0,
		//WorkerLocalActivitiesPerSecond:          0,
		//TaskQueueActivitiesPerSecond:            0,
		//MaxConcurrentActivityTaskPollers:        0,
		//MaxConcurrentWorkflowTaskExecutionSize:  0,
		//MaxConcurrentWorkflowTaskPollers:        0,
		//EnableLoggingInReplay:                   false,
		//StickyScheduleToStartTimeout:            0,
		//BackgroundActivityContext:               nil,
		//WorkflowPanicPolicy:                     0,
		//WorkerStopTimeout:                       0,
		//EnableSessionWorker:                     false,
		//MaxConcurrentSessionExecutionSize:       0,
		//DisableWorkflowWorker:                   false,
		//LocalActivityWorkerOnly:                 false,
		//Identity:                                "",
		//DeadlockDetectionTimeout:                0,
		//MaxHeartbeatThrottleInterval:            0,
		//DefaultHeartbeatThrottleInterval:        0,
		//Interceptors:                            nil,
		//OnFatalError:                            nil,
		//DisableEagerActivities:                  false,
		//MaxConcurrentEagerActivityExecutionSize: 0,
		//DisableRegistrationAliasing:             false,
		//BuildID:                                 "",
		//UseBuildIDForVersioning:                 false,
	})
	e := &Engine{
		log:       opts.Log,
		tc:        opts.TC,
		taskQueue: opts.TaskQueue,
		ws: &workflowState{
			activities: &activityState{
				source: &gitalySource{
					gitalyPool:    opts.GitalyPool,
					gitLabClient:  opts.GitLabClient,
					repoInfoCache: opts.RepoInfoCache,
					maxFileSize:   maxFileSize,
				},
				httpRT: opts.HTTPRT,
			},
			vars: starlark.StringDict{
				varGitLabURL: starlark.String(opts.GitLabURL),
			},
		},
		tw: tw,
	}
	tw.RegisterWorkflow(e.ws.HandleEventWorkflow)
	tw.RegisterWorkflow(e.ws.RunEventHandlerWorkflow)
	tw.RegisterActivity(e.ws.activities)
	return e, nil
}

func (e *Engine) Run(ctx context.Context) error {
	if err := e.tw.Start(); err != nil {
		return err
	}
	<-ctx.Done()
	e.tw.Stop()
	return nil
}

func (e *Engine) Event(ctx context.Context, req *rpc.CloudEventRequest) error {
	wopts := tclient.StartWorkflowOptions{
		TaskQueue: e.taskQueue,
	}
	_, err := e.tc.ExecuteWorkflow(ctx, wopts, e.ws.HandleEventWorkflow, &HandleEventWorkflowInput{
		Event:       req.Event,
		FlowProject: req.FlowProject,
		Entrypoint:  defaultEntrypoint,
	})
	if err != nil {
		return err
	}
	return nil
}
