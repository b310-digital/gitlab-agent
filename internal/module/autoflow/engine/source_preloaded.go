package engine

import (
	"context"
	"fmt"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow/flow"
	"go.starlark.net/starlark"
)

var (
	_ flow.Source = (*preloadedSource)(nil)
)

type preloadedSource struct {
	modules []*ModuleSource
}

func (p *preloadedSource) SetupThreadLocal(thread *starlark.Thread, projectPath string) {
}

func (p *preloadedSource) RecordSource(thread *starlark.Thread, name, projectPath, fullGitRef string) error {
	return nil
}

func (p *preloadedSource) LoadFile(ctx context.Context, thread *starlark.Thread, ref flow.ModuleRef) ([]byte, error) {
	for _, module := range p.modules {
		if ref.SourceName == module.SourceName && ref.File == module.File {
			return module.Data, nil
		}
	}
	return nil, fmt.Errorf("unknown module: %s", ref)
}
