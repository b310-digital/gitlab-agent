package engine

import (
	"context"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow/flow"
	"go.starlark.net/starlark"
)

var (
	_ flow.Source = (*recordingSource)(nil)
)

type recordingSource struct {
	delegate flow.Source
	modules  []*ModuleSource
}

func newRecordingSource(delegate flow.Source) *recordingSource {
	return &recordingSource{
		delegate: delegate,
	}
}

func (r *recordingSource) SetupThreadLocal(thread *starlark.Thread, projectPath string) {
	r.delegate.SetupThreadLocal(thread, projectPath)
}

func (r *recordingSource) RecordSource(thread *starlark.Thread, name, projectPath, fullGitRef string) error {
	return r.delegate.RecordSource(thread, name, projectPath, fullGitRef)
}

func (r *recordingSource) LoadFile(ctx context.Context, thread *starlark.Thread, ref flow.ModuleRef) ([]byte, error) {
	data, err := r.delegate.LoadFile(ctx, thread, ref)
	if err != nil {
		return nil, err
	}
	r.modules = append(r.modules, &ModuleSource{
		SourceName: ref.SourceName,
		File:       ref.File,
		Data:       data,
	})
	return data, nil
}
