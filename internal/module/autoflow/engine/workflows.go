package engine

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow/flow"
	"go.starlark.net/starlark"
	enumspb "go.temporal.io/api/enums/v1"
	"go.temporal.io/sdk/workflow"
)

// workflowState holds shared state that is used by all Temporal workflows.
type workflowState struct {
	activities *activityState
	vars       starlark.StringDict
}

func (w *workflowState) HandleEventWorkflow(ctx workflow.Context, input *HandleEventWorkflowInput) (*HandleEventWorkflowOutput, error) {
	activityCtx := workflow.WithActivityOptions(ctx, workflow.ActivityOptions{
		// TODO
		StartToCloseTimeout: 10 * time.Second,
	})

	f := workflow.ExecuteActivity(activityCtx, w.activities.LoadAllEventHandlersActivity, &LoadAllEventHandlersActivityInput{
		Event:       input.Event,
		FlowProject: input.FlowProject,
		Entrypoint:  input.Entrypoint,
	})
	var output LoadAllEventHandlersActivityOutput
	err := f.Get(ctx, &output)
	if err != nil {
		//logger.Error("Activity failed.", "Error", err)
		// TODO
		return &HandleEventWorkflowOutput{}, err
	}

	nHandlers := len(output.Handlers)
	log := workflow.GetLogger(ctx)
	if nHandlers == 0 {
		log.Debug("No handlers registered for event type",
			"event_type", input.Event.Type)
		return nil, nil
	}
	log.Debug(fmt.Sprintf("Starting %d child workflows for registered event handlers", nHandlers),
		"event_type", input.Event.Type)

	// We spawn a child workflow for each event handler, but we don't want to block this workflow until those handlers
	// finish executing as that may take an arbitrary amount of time. So we:
	// 1. Set ParentClosePolicy to PARENT_CLOSE_POLICY_ABANDON to avoid canceling child workflows when
	//    parent (i.e. this workflow) exits.
	// 2. Run ExecuteChildWorkflow() for each handler.
	// 3. Wait for all child workflows to emit ChildWorkflowExecutionStarted event. Must wait for the event, otherwise
	//    child workflows don't start.

	futures := make([]workflow.Future, 0, nHandlers)
	workflowIDPrefix := fmt.Sprintf("project_%d/", input.FlowProject.Id)
	for handler, h := range output.Handlers {
		id := h.WorkflowId
		if id != "" {
			id = workflowIDPrefix + id
		}
		childCtx := workflow.WithChildOptions(ctx, workflow.ChildWorkflowOptions{
			// TODO
			WorkflowID:        id,
			ParentClosePolicy: enumspb.PARENT_CLOSE_POLICY_ABANDON,
		})
		fut := workflow.ExecuteChildWorkflow(childCtx, w.RunEventHandlerWorkflow, &RunEventHandlerWorkflowInput{
			Event:            input.Event,
			FlowProject:      input.FlowProject,
			Entrypoint:       input.Entrypoint,
			Modules:          output.Modules,
			Handler:          uint32(handler),
			WorkflowIdPrefix: workflowIDPrefix,
		})
		futures = append(futures, fut.GetChildWorkflowExecution())
	}
	for _, fut := range futures {
		err = fut.Get(ctx, nil)
		if err != nil {
			return nil, err
		}
	}
	return &HandleEventWorkflowOutput{}, nil
}

func (w *workflowState) RunEventHandlerWorkflow(ctx workflow.Context, input *RunEventHandlerWorkflowInput) (*RunEventHandlerWorkflowOutput, error) {
	src := &preloadedSource{
		modules: input.Modules,
	}
	log := workflow.GetLogger(ctx)
	runtime, err := flow.NewRuntimeFromSource(context.Background(), flow.RuntimeOptions{
		Source:      src,
		ProjectPath: input.FlowProject.FullPath,
		Entrypoint:  input.Entrypoint,
		PrintFunc: func(t *starlark.Thread, msg string) {
			log.Info(msg)
		},
		NowFunc: func() time.Time {
			return workflow.Now(ctx)
		},
	})
	if err != nil {
		return nil, fmt.Errorf("failed to create flow runtime: %w", err)
	}

	fc := &flowContext{
		ctx:              ctx,
		activities:       w.activities,
		workflowIDPrefix: input.WorkflowIdPrefix,
	}
	e, err := eventToDict(input.Event)
	if err != nil {
		return nil, fmt.Errorf("eventToDict: %w", err)
	}
	err = runtime.Execute(input.Event.Type, input.Handler, fc, w.vars, e)
	if err != nil {
		return nil, err
	}

	return &RunEventHandlerWorkflowOutput{}, nil
}
