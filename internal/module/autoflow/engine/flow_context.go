package engine

import (
	"errors"
	"fmt"
	"net/url"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow/flow"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/httpz"
	startime "go.starlark.net/lib/time"
	"go.starlark.net/starlark"
	"go.starlark.net/starlarkstruct"
	"go.temporal.io/sdk/temporal"
	"go.temporal.io/sdk/workflow"
)

var (
	_ flow.EventHandlerContext = (*flowContext)(nil)
)

type flowContext struct {
	ctx              workflow.Context
	activities       *activityState
	workflowIDPrefix string
}

func (f *flowContext) HTTPDo(reqURL string, method string, query *starlark.Dict, header *starlark.Dict, body starlark.Bytes) (starlark.Value, error) {
	headerMap, err := dict2valuesMap(header)
	if err != nil {
		return nil, err
	}
	u, err := url.Parse(reqURL)
	if err != nil {
		return nil, err
	}
	values, err := dict2urlValues(query)
	if err != nil {
		return nil, err
	}

	activityCtx := workflow.WithActivityOptions(f.ctx, workflow.ActivityOptions{
		// TODO
		StartToCloseTimeout: 10 * time.Second,
	})
	future := workflow.ExecuteActivity(activityCtx, f.activities.HTTPDoActivity, &HttpDoActivityInput{
		Method: method,
		Url:    httpz.MergeURLPathAndQuery(u, "", values),
		Header: headerMap,
		Body:   []byte(body),
	})
	var output HttpDoActivityOutput
	err = future.Get(f.ctx, &output)
	if err != nil {
		return nil, err
	}

	resHeaders, err := valuesMap2dict(output.Header)
	if err != nil {
		return nil, err
	}
	return &starlarkstruct.Module{
		Name: "http_response",
		Members: starlark.StringDict{
			"status_code": starlark.MakeUint64(uint64(output.StatusCode)),
			"status":      starlark.String(output.Status),
			"header":      resHeaders,
			"body":        starlark.Bytes(output.Body),
		},
	}, err
}

func (f *flowContext) Select(cases *starlark.Dict, defaultCase starlark.Value, timeout startime.Duration, timeoutValue starlark.Value) (starlark.Value, error) {
	var selected starlark.Value
	selector := workflow.NewSelector(f.ctx)
	for _, kw := range cases.Items() {
		k, ok := kw[0].(starlark.String)
		if !ok {
			return nil, fmt.Errorf("key must be a string, got %s", kw[0].Type())
		}
		v := kw[1]
		switch v := v.(type) {
		case *flow.FutureType:
			future, ok := v.Unwrap().(workflow.Future)
			if !ok {
				return nil, errors.New("internal error: future type did not return a wrapped workflow future")
			}
			selector.AddFuture(future, func(fut workflow.Future) {
				selected = k
			})
		case *flow.ReceiveChannelType:
			channel, ok := v.Unwrap().(workflow.ReceiveChannel)
			if !ok {
				return nil, errors.New("internal error: receive channel type did not return a wrapped workflow receive channel")
			}
			selector.AddReceive(channel, func(c workflow.ReceiveChannel, more bool) {
				selected = k
			})
		default:
			return nil, fmt.Errorf("unsupported value type %s for the select key %q", v.Type(), k)
		}
	}
	if defaultCase != starlark.None {
		selector.AddDefault(func() {
			selected = defaultCase
		})
	}
	if timeout != 0 {
		ctx, cancelFunc := workflow.WithCancel(f.ctx)
		defer cancelFunc()
		t := workflow.NewTimer(ctx, time.Duration(timeout))
		selector.AddFuture(t, func(f workflow.Future) {
			selected = timeoutValue
		})
	}
	selector.Select(f.ctx)
	return selected, nil
}

func (f *flowContext) Signal(workflowID string, signalName string, payloadJSON starlark.Value, runID string) (starlark.Value, error) {
	err := workflow.SignalExternalWorkflow(f.ctx, f.workflowIDPrefix+workflowID, runID, signalName, &SignalPayload{
		Payload: string(payloadJSON.(starlark.String)),
	}).Get(f.ctx, nil)
	switch err.(type) { //nolint:errorlint
	case nil:
		return starlark.True, nil
	case *temporal.UnknownExternalWorkflowExecutionError:
		return starlark.False, nil
	default:
		// TODO inspect other errors?
		return nil, err
	}
}

func (f *flowContext) SignalChannel(signalName string) flow.ReceiveChannel {
	return &receiveChannel{
		ctx: f.ctx,
		c:   workflow.GetSignalChannel(f.ctx, signalName),
	}
}

var (
	_ flow.ReceiveChannel = (*receiveChannel)(nil)
)

type receiveChannel struct {
	ctx workflow.Context
	c   workflow.ReceiveChannel
}

func (c *receiveChannel) Receive(payloadJSON *string) bool {
	return c.c.Receive(c.ctx, payloadJSON)
}

func (c *receiveChannel) Unwrap() any {
	return c.c
}

func (f *flowContext) Sleep(duration startime.Duration) (starlark.Value, error) {
	err := workflow.Sleep(f.ctx, time.Duration(duration))
	if err != nil {
		return nil, err
	}
	return starlark.None, nil
}

func (f *flowContext) Timer(duration startime.Duration) flow.Future {
	return &timer{
		ctx: f.ctx,
		f:   workflow.NewTimer(f.ctx, time.Duration(duration)),
	}
}

var (
	_ flow.Future = (*timer)(nil)
)

type timer struct {
	ctx workflow.Context
	f   workflow.Future
}

func (t *timer) Get() error {
	return t.f.Get(t.ctx, nil)
}

func (t *timer) Unwrap() any {
	return t.f
}

func valuesMap2dict(m map[string]*Values) (*starlark.Dict, error) {
	d := starlark.NewDict(len(m))

	for k, v := range m {
		vals := make([]starlark.Value, 0, len(v.Value))
		for _, val := range v.Value {
			vals = append(vals, starlark.String(val))
		}
		err := d.SetKey(starlark.String(k), starlark.NewList(vals))
		if err != nil {
			return nil, err
		}
	}

	d.Freeze()
	return d, nil
}

func dict2valuesMap(d *starlark.Dict) (map[string]*Values, error) {
	if d == nil || d.Len() == 0 {
		return nil, nil
	}
	// TODO Use more efficient iteration if/when available https://github.com/google/starlark-go/issues/518
	items := d.Items()
	res := make(map[string]*Values, len(items))
	for _, item := range items {
		key, ok := item[0].(starlark.String)
		if !ok {
			return nil, fmt.Errorf("key must be a string, got %T", item[0])
		}

		var resVal []string
		switch val := item[1].(type) {
		case *starlark.List:
			l := val.Len()
			resVal = make([]string, 0, l)
			for i := 0; i < l; i++ {
				listItem := val.Index(i)
				listItemStr, ok := listItem.(starlark.String)
				if !ok {
					return nil, fmt.Errorf("value must be a string, got %T", listItem)
				}
				resVal = append(resVal, string(listItemStr))
			}
		case starlark.String:
			resVal = []string{string(val)}
		default:
			return nil, fmt.Errorf("value must be a string or a list of strings, got %T", item[1])
		}
		res[string(key)] = &Values{Value: resVal}
	}

	return res, nil
}

func dict2urlValues(d *starlark.Dict) (url.Values, error) {
	if d == nil || d.Len() == 0 {
		return nil, nil
	}
	// TODO Use more efficient iteration if/when available https://github.com/google/starlark-go/issues/518
	items := d.Items()
	res := make(url.Values, len(items))
	for _, item := range items {
		key, ok := item[0].(starlark.String)
		if !ok {
			return nil, fmt.Errorf("key must be a string, got %T", item[0])
		}

		var resVal []string
		var err error
		switch val := item[1].(type) { // check for concrete types as we don't want to allow Dict as Sequence
		case *starlark.Set:
			resVal, err = sequenceToStringSlice(val)
			if err != nil {
				return nil, err
			}
		case *starlark.List:
			resVal, err = sequenceToStringSlice(val)
			if err != nil {
				return nil, err
			}
		case starlark.Tuple:
			resVal, err = sequenceToStringSlice(val)
			if err != nil {
				return nil, err
			}
		case starlark.String:
			resVal = []string{string(val)}
		default:
			return nil, fmt.Errorf("value must be a string or a list of strings, got %T", item[1])
		}
		res[string(key)] = resVal
	}

	return res, nil
}

func sequenceToStringSlice(s starlark.Sequence) ([]string, error) {
	l := s.Len()
	res := make([]string, 0, l)
	iter := s.Iterate()
	defer iter.Done()
	var item starlark.Value
	for iter.Next(&item) {
		itemStr, ok := item.(starlark.String)
		if !ok {
			return nil, fmt.Errorf("value must be a string, got %T", item)
		}
		res = append(res, string(itemStr))
	}
	return res, nil
}
