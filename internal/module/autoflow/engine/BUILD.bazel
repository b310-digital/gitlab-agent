load("@io_bazel_rules_go//go:def.bzl", "go_library")
load("//build:build.bzl", "go_custom_test")
load("//build:proto.bzl", "go_proto_generate")

go_proto_generate(
    src = "engine.proto",
    deps = [
        "//pkg/event:proto",
    ],
)

go_library(
    name = "engine",
    srcs = [
        "activities.go",
        "engine.go",
        "engine.pb.go",
        "flow_context.go",
        "source_gitaly.go",
        "source_preloaded.go",
        "source_recording.go",
        "tools.go",
        "workflows.go",
    ],
    importpath = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow/engine",
    visibility = ["//:__subpackages__"],
    deps = [
        "//internal/api",
        "//internal/gitaly",
        "//internal/gitlab",
        "//internal/gitlab/api",
        "//internal/module/autoflow/flow",
        "//internal/module/autoflow/rpc",
        "//internal/tool/cache",
        "//internal/tool/errz",
        "//internal/tool/httpz",
        "//pkg/event",
        "@io_temporal_go_api//enums/v1:enums",
        "@io_temporal_go_sdk//activity",
        "@io_temporal_go_sdk//client",
        "@io_temporal_go_sdk//temporal",
        "@io_temporal_go_sdk//worker",
        "@io_temporal_go_sdk//workflow",
        "@net_starlark_go//lib/json",
        "@net_starlark_go//lib/time",
        "@net_starlark_go//starlark",
        "@net_starlark_go//starlarkstruct",
        "@org_golang_google_protobuf//proto",
        "@org_golang_google_protobuf//reflect/protoreflect",
        "@org_golang_google_protobuf//runtime/protoimpl",
        "@org_golang_google_protobuf//types/known/anypb",
        "@org_golang_google_protobuf//types/known/timestamppb",
    ],
)

go_custom_test(
    name = "engine_test",
    srcs = ["tools_test.go"],
    embed = [":engine"],
    deps = [
        "//pkg/event",
        "@com_github_stretchr_testify//assert",
        "@com_github_stretchr_testify//require",
        "@net_starlark_go//lib/time",
        "@net_starlark_go//starlark",
        "@org_golang_google_protobuf//proto",
        "@org_golang_google_protobuf//types/known/anypb",
        "@org_golang_google_protobuf//types/known/timestamppb",
    ],
)
