package flow

import (
	"fmt"
	"net/http"

	starjson "go.starlark.net/lib/json"
	startime "go.starlark.net/lib/time"
	"go.starlark.net/starlark"
	"go.starlark.net/starlarkstruct"
)

const (
	selectBuiltin        = "select"
	signalBuiltin        = "signal"
	signalChannelBuiltin = "signal_channel"
	sleepBuiltin         = "sleep"
	timerBuiltin         = "timer"
)

// TODO: eventually abstract starlark types away ... maybe?!
type EventHandlerContext interface {
	HTTPDo(reqURL string, method string, query *starlark.Dict, header *starlark.Dict, body starlark.Bytes) (starlark.Value, error)
	Select(cases *starlark.Dict, defaultCase starlark.Value, timeout startime.Duration, timeoutValue starlark.Value) (starlark.Value, error)
	Signal(workflowID string, signalName string, payloadJSON starlark.Value, runID string) (starlark.Value, error)
	SignalChannel(signalName string) ReceiveChannel
	Sleep(duration startime.Duration) (starlark.Value, error)
	Timer(duration startime.Duration) Future
}

type eventHandlerContextModule struct {
	ctx  EventHandlerContext
	vars starlark.StringDict
}

func (m *eventHandlerContextModule) toStarlarkModule() *starlarkstruct.Module {
	w := &starlarkstruct.Module{
		Name: "eventHandlerContext",
		Members: starlark.StringDict{
			"http": &starlarkstruct.Module{
				Name: "http",
				Members: starlark.StringDict{
					"do": starlark.NewBuiltin("http_do", m.httpDo),
				},
			},
			"vars": &starlarkstruct.Module{
				Name:    "vars",
				Members: m.vars,
			},
			selectBuiltin:        starlark.NewBuiltin(selectBuiltin, m.selectFunc),
			signalBuiltin:        starlark.NewBuiltin(signalBuiltin, m.signal),
			signalChannelBuiltin: starlark.NewBuiltin(signalChannelBuiltin, m.signalChannel),
			sleepBuiltin:         starlark.NewBuiltin(sleepBuiltin, m.sleep),
			timerBuiltin:         starlark.NewBuiltin(timerBuiltin, m.timer),
		},
	}
	w.Freeze()
	return w
}

func (m *eventHandlerContextModule) httpDo(thread *starlark.Thread, fn *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	if len(args) > 0 {
		return nil, fmt.Errorf("%s: unexpected positional arguments", fn.Name())
	}
	var (
		reqURL string
		method = http.MethodGet
		query  *starlark.Dict
		header *starlark.Dict
		body   starlark.Bytes
	)
	err := starlark.UnpackArgs(fn.Name(), nil, kwargs,
		"url", &reqURL,
		"method??", &method,
		"query??", &query,
		"header??", &header,
		"body??", &body,
	)
	if err != nil {
		return nil, err
	}

	return m.ctx.HTTPDo(reqURL, method, query, header, body)
}

func (m *eventHandlerContextModule) selectFunc(thread *starlark.Thread, fn *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	var (
		cases        *starlark.Dict
		defaultCase  starlark.Value = starlark.None
		timeout      startime.Duration
		timeoutValue starlark.Value = starlark.False
	)
	err := starlark.UnpackPositionalArgs(fn.Name(), args, nil, 1, &cases)
	if err != nil {
		return nil, err
	}
	err = starlark.UnpackArgs(fn.Name(), nil, kwargs,
		"default??", &defaultCase,
		"timeout??", &timeout,
		"timeout_value??", &timeoutValue,
	)
	if err != nil {
		return nil, err
	}

	return m.ctx.Select(cases, defaultCase, timeout, timeoutValue)
}

func (m *eventHandlerContextModule) signal(thread *starlark.Thread, fn *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	if len(args) > 0 {
		return nil, fmt.Errorf("%s: unexpected positional arguments", fn.Name())
	}

	var (
		workflowID, runID, signalName string
		payload                       starlark.Value = starlark.None
	)

	err := starlark.UnpackArgs(fn.Name(), nil, kwargs,
		"workflow_id", &workflowID,
		"signal_name", &signalName,
		"payload?", &payload,
		"run_id??", &runID,
	)
	if err != nil {
		return nil, err
	}

	payloadJSON, err := starlark.Call(thread, starjson.Module.Members["encode"], starlark.Tuple{payload}, nil)
	if err != nil {
		return nil, err
	}

	return m.ctx.Signal(workflowID, signalName, payloadJSON, runID)
}

func (m *eventHandlerContextModule) signalChannel(thread *starlark.Thread, fn *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	var signalName starlark.String
	err := starlark.UnpackPositionalArgs(fn.Name(), args, kwargs, 1, &signalName)
	if err != nil {
		return nil, err
	}

	return newReceiveChannel(m.ctx.SignalChannel(string(signalName))), nil
}

func (m *eventHandlerContextModule) sleep(thread *starlark.Thread, fn *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	var d startime.Duration
	err := starlark.UnpackPositionalArgs(fn.Name(), args, kwargs, 1, &d)
	if err != nil {
		return nil, err
	}

	return m.ctx.Sleep(d)
}

func (m *eventHandlerContextModule) timer(thread *starlark.Thread, fn *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	var d startime.Duration
	err := starlark.UnpackPositionalArgs(fn.Name(), args, kwargs, 1, &d)
	if err != nil {
		return nil, err
	}

	return &FutureType{f: m.ctx.Timer(d)}, nil
}
