package server

import (
	"crypto/tls"
	"net"
	"net/http"
	"strings"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	gapi "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow/engine"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/cache"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/prototool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/redistool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/tlstool"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	tclient "go.temporal.io/sdk/client"
	"google.golang.org/grpc"
	"google.golang.org/grpc/stats"
)

const (
	kasWorkerTaskQueue = "kas-worker"
)

type Factory struct {
	CSH              stats.Handler
	StreamClientProm grpc.StreamClientInterceptor
	UnaryClientProm  grpc.UnaryClientInterceptor
}

func (f *Factory) New(config *modserver.Config) (modserver.Module, error) {
	cfg := config.Config.Autoflow
	if cfg == nil {
		config.Log.Info("AutoFlow module is not enabled in config")
		return nil, nil
	}

	var tlsConfig *tls.Config
	if cfg.Temporal.EnableTls {
		var err error
		tlsConfig, err = tlstool.DefaultClientTLSConfigWithCACertKeyPair(
			cfg.Temporal.GetCaCertificateFile(),
			cfg.Temporal.GetCertificateFile(),
			cfg.Temporal.GetKeyFile(),
		)
		if err != nil {
			return nil, err
		}
	}
	tc, err := tclient.Dial(tclient.Options{
		HostPort:  cfg.Temporal.HostPort,
		Namespace: cfg.Temporal.GetNamespace(), // empty string means default value, which is "default"
		Logger:    config.Log,
		//MetricsHandler: nil,
		//Identity:           "",
		//DataConverter:      nil,
		//FailureConverter:   nil,
		//ContextPropagators: nil,
		ConnectionOptions: tclient.ConnectionOptions{
			TLS: tlsConfig,
			//Authority:                    "",
			//EnableKeepAliveCheck:         false,
			//KeepAliveTime:                0,
			//KeepAliveTimeout:             0,
			//KeepAlivePermitWithoutStream: false,
			//MaxPayloadSize:               0,
			DialOptions: []grpc.DialOption{
				grpc.WithStatsHandler(otelgrpc.NewServerHandler(
					otelgrpc.WithTracerProvider(config.TraceProvider),
					otelgrpc.WithMeterProvider(config.MeterProvider),
					otelgrpc.WithPropagators(config.TracePropagator),
					otelgrpc.WithMessageEvents(otelgrpc.ReceivedEvents, otelgrpc.SentEvents),
				)),
				grpc.WithStatsHandler(f.CSH),
				grpc.WithChainStreamInterceptor(
					f.StreamClientProm,
				),
				grpc.WithChainUnaryInterceptor(
					f.UnaryClientProm,
				),
				grpc.WithSharedWriteBuffer(true),
			},
		},
		//HeadersProvider:   nil,
		//TrafficController: nil,
		//Interceptors:      nil,
	})
	if err != nil {
		return nil, err
	}

	flowEng, err := engine.NewEngine(engine.Options{
		Log:          config.Log,
		TC:           tc,
		GitalyPool:   config.Gitaly,
		GitLabClient: config.GitLabClient,
		RepoInfoCache: cache.NewWithError[engine.RepoInfoCacheKey, *api.ProjectInfo](
			cfg.ProjectInfoCacheTtl.AsDuration(),
			cfg.ProjectInfoCacheErrorTtl.AsDuration(),
			&redistool.ErrCacher[engine.RepoInfoCacheKey]{
				Log:          config.Log,
				ErrRep:       modshared.APIToErrReporter(config.API),
				Client:       config.RedisClient,
				ErrMarshaler: prototool.ProtoErrMarshaler{},
				KeyToRedisKey: func(cacheKey engine.RepoInfoCacheKey) string {
					return config.Config.Redis.KeyPrefix +
						":repo_info_errs:" +
						cacheKey.ProjectID
				},
			},
			config.TraceProvider.Tracer(autoflow.ModuleName),
			gapi.IsCacheableError,
		),
		HTTPRT: otelhttp.NewTransport(
			&http.Transport{
				Proxy: http.ProxyFromEnvironment,
				DialContext: (&net.Dialer{
					Timeout:   30 * time.Second,
					KeepAlive: 30 * time.Second,
				}).DialContext,
				TLSClientConfig:       tlstool.DefaultClientTLSConfig(),
				MaxIdleConns:          10,
				IdleConnTimeout:       90 * time.Second,
				TLSHandshakeTimeout:   10 * time.Second,
				ResponseHeaderTimeout: 20 * time.Second,
				ForceAttemptHTTP2:     true,
			},
			otelhttp.WithPropagators(config.TracePropagator),
			otelhttp.WithTracerProvider(config.TraceProvider),
			otelhttp.WithMeterProvider(config.MeterProvider),
		),
		TaskQueue: kasWorkerTaskQueue,
		GitLabURL: strings.TrimSuffix(config.Config.Gitlab.ExternalURL(), "/"),
	})
	if err != nil {
		return nil, err
	}
	srv := &server{
		log:       config.Log,
		serverAPI: config.API,
		flowEng:   flowEng,
	}
	rpc.RegisterAutoFlowServer(config.APIServer, srv)
	return &module{
		flowEng: flowEng,
		tc:      tc,
	}, nil
}

func (f *Factory) Name() string {
	return autoflow.ModuleName
}

func (f *Factory) StartStopPhase() modshared.ModuleStartStopPhase {
	return modshared.ModuleStartBeforeServers
}
