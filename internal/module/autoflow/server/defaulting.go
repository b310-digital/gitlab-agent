package server

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/prototool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/kascfg"
)

func ApplyDefaults(config *kascfg.ConfigurationFile) {
	if config.Autoflow == nil { // module disabled. This is temporary.
		return
	}
	prototool.NotNil(&config.Autoflow.Temporal)
	//t := config.Automation.Temporal
	//
	//prototool.StringPtr(&t.Namespace, "")
	//prototool.StringPtr(&t.CaCertificateFile, "")
	//prototool.StringPtr(&t.CertificateFile, "")
	//prototool.StringPtr(&t.KeyFile, "")
}
