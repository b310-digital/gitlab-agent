package server

import (
	"context"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow/engine"
	tclient "go.temporal.io/sdk/client"
)

type module struct {
	flowEng *engine.Engine
	tc      tclient.Client
}

func (m *module) Run(ctx context.Context) error {
	defer m.tc.Close()
	return m.flowEng.Run(ctx)
}

func (m *module) Name() string {
	return autoflow.ModuleName
}
