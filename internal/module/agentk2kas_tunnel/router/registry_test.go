package router

import (
	"context"
	"io"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/prototool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/matcher"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_agentk2kas_router"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_reverse_tunnel_rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_tunnel_tunserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/info"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/tunserver"
	"go.opentelemetry.io/otel/trace"
	"go.opentelemetry.io/otel/trace/noop"
	"go.uber.org/mock/gomock"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"k8s.io/apimachinery/pkg/util/wait"
)

// "slow" tests in this file are marked for concurrent execution with t.Parallel()

const (
	serviceName    = "gitlab.service1"
	methodName     = "DoSomething"
	fullMethodName = "/" + serviceName + "/" + methodName
)

var (
	_ Handler = &Registry{}
	_ Querier = &Registry{}
)

func TestStopUnregistersAllConnections(t *testing.T) {
	t.Parallel()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	ctrl := gomock.NewController(t)
	mockAPI := mock_modshared.NewMockAPI(ctrl)
	connectServer := mock_reverse_tunnel_rpc.NewMockReverseTunnel_ConnectServer(ctrl)
	tunnelTracker := mock_agentk2kas_router.NewMockTracker(ctrl)
	connectServer.EXPECT().
		Context().
		Return(context.Background()).
		MinTimes(1)
	gomock.InOrder(
		connectServer.EXPECT().
			Recv().
			Return(&rpc.ConnectRequest{
				Msg: &rpc.ConnectRequest_Descriptor_{
					Descriptor_: descriptor(),
				},
			}, nil),
		tunnelTracker.EXPECT().
			RegisterTunnel(gomock.Any(), gomock.Any(), testhelpers.AgentID).
			Do(func(ctx context.Context, duration time.Duration, agentID int64) error {
				cancel()
				return nil
			}),
		tunnelTracker.EXPECT().
			UnregisterTunnel(gomock.Any(), testhelpers.AgentID),
	)
	mockAPI.EXPECT().
		HandleProcessingError(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any())
	r := NewRegistry(testhelpers.NewLogger(t), mockAPI, nt(), time.Minute, time.Minute, time.Minute, tunnelTracker)
	var wg wait.Group
	defer wg.Wait()
	wg.Start(func() {
		err := r.HandleTunnel(context.Background(), testhelpers.AgentInfoObj(), connectServer)
		assert.NoError(t, err)
	})
	err := r.Run(ctx)
	assert.NoError(t, err)
}

func TestTunnelDoneRegistersUnusedTunnel(t *testing.T) {
	t.Parallel()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	ctrl := gomock.NewController(t)
	mockAPI := mock_modshared.NewMockAPI(ctrl)
	connectServer := mock_reverse_tunnel_rpc.NewMockReverseTunnel_ConnectServer(ctrl)
	tunnelTracker := mock_agentk2kas_router.NewMockTracker(ctrl)
	connectServer.EXPECT().
		Context().
		Return(context.Background()).
		MinTimes(1)
	reg := make(chan struct{})
	gomock.InOrder(
		connectServer.EXPECT().
			Recv().
			Return(&rpc.ConnectRequest{
				Msg: &rpc.ConnectRequest_Descriptor_{
					Descriptor_: descriptor(),
				},
			}, nil),
		tunnelTracker.EXPECT(). // HandleTunnel()
					RegisterTunnel(gomock.Any(), gomock.Any(), gomock.Any()).
					Do(func(ctx context.Context, ttl time.Duration, agentID int64) error {
				close(reg)
				return nil
			}),
		tunnelTracker.EXPECT(). // FindTunnel()
					UnregisterTunnel(gomock.Any(), gomock.Any()),
		tunnelTracker.EXPECT(). // Done()
					RegisterTunnel(gomock.Any(), gomock.Any(), gomock.Any()),
		tunnelTracker.EXPECT(). // FindTunnel()
					UnregisterTunnel(gomock.Any(), gomock.Any()),
		tunnelTracker.EXPECT(). // Done()
					RegisterTunnel(gomock.Any(), gomock.Any(), gomock.Any()),
		tunnelTracker.EXPECT(). // stopInternal()
					UnregisterTunnel(gomock.Any(), gomock.Any()),
	)
	mockAPI.EXPECT().
		HandleProcessingError(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any())

	agentInfo := testhelpers.AgentInfoObj()
	r := NewRegistry(testhelpers.NewLogger(t), mockAPI, nt(), time.Minute, time.Minute, time.Minute, tunnelTracker)
	var wg wait.Group
	defer wg.Wait()
	wg.Start(func() {
		err := r.HandleTunnel(context.Background(), agentInfo, connectServer)
		assert.NoError(t, err)
	})
	wg.Start(func() {
		err := r.Run(ctx)
		assert.NoError(t, err)
	})
	<-reg
	found, th := r.FindTunnel(context.Background(), agentInfo.ID, serviceName, methodName)
	assert.True(t, found)
	tun, err := th.Get(context.Background())
	require.NoError(t, err)
	tun.Done(context.Background())
	th.Done(context.Background())
	found, th = r.FindTunnel(context.Background(), agentInfo.ID, serviceName, methodName)
	assert.True(t, found)
	tun, err = th.Get(context.Background())
	require.NoError(t, err)
	tun.Done(context.Background())
	th.Done(context.Background())
	cancel()
}

func TestTunnelDoneDonePanics(t *testing.T) {
	t.Parallel()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	ctrl := gomock.NewController(t)
	mockAPI := mock_modshared.NewMockAPI(ctrl)
	connectServer := mock_reverse_tunnel_rpc.NewMockReverseTunnel_ConnectServer(ctrl)
	tunnelTracker := mock_agentk2kas_router.NewMockTracker(ctrl)
	connectServer.EXPECT().
		Context().
		Return(context.Background()).
		MinTimes(1)
	reg := make(chan struct{})
	connectServer.EXPECT().
		Recv().
		Return(&rpc.ConnectRequest{
			Msg: &rpc.ConnectRequest_Descriptor_{
				Descriptor_: descriptor(),
			},
		}, nil)
	gomock.InOrder(
		tunnelTracker.EXPECT().
			RegisterTunnel(gomock.Any(), gomock.Any(), gomock.Any()).
			Do(func(ctx context.Context, ttl time.Duration, agentID int64) error {
				close(reg)
				return nil
			}),
		tunnelTracker.EXPECT().
			UnregisterTunnel(gomock.Any(), gomock.Any()),
		tunnelTracker.EXPECT().
			RegisterTunnel(gomock.Any(), gomock.Any(), gomock.Any()),
		tunnelTracker.EXPECT().
			UnregisterTunnel(gomock.Any(), gomock.Any()),
	)
	mockAPI.EXPECT().
		HandleProcessingError(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any())
	agentInfo := testhelpers.AgentInfoObj()
	r := NewRegistry(testhelpers.NewLogger(t), mockAPI, nt(), time.Minute, time.Minute, time.Minute, tunnelTracker)
	var wg wait.Group
	defer wg.Wait()
	wg.Start(func() {
		err := r.HandleTunnel(context.Background(), agentInfo, connectServer)
		assert.NoError(t, err)
	})
	wg.Start(func() {
		err := r.Run(ctx)
		assert.NoError(t, err)
	})
	<-reg
	found, th := r.FindTunnel(context.Background(), agentInfo.ID, serviceName, methodName)
	assert.True(t, found)
	tun, err := th.Get(context.Background())
	require.NoError(t, err)
	tun.Done(context.Background())
	assert.PanicsWithError(t, "unreachable: ready -> done should never happen", func() {
		tun.Done(context.Background())
	})
	th.Done(context.Background())
	cancel()
}

func TestHandleTunnelIsUnblockedByContext(t *testing.T) {
	t.Parallel()
	ctxConn, cancelConn := context.WithTimeout(context.Background(), 50*time.Millisecond) // will unblock HandleTunnel()
	defer cancelConn()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	ctrl := gomock.NewController(t)
	mockAPI := mock_modshared.NewMockAPI(ctrl)
	connectServer := mock_reverse_tunnel_rpc.NewMockReverseTunnel_ConnectServer(ctrl)
	tunnelTracker := mock_agentk2kas_router.NewMockTracker(ctrl)
	connectServer.EXPECT().
		Context().
		Return(context.Background()).
		MinTimes(1)
	gomock.InOrder(
		connectServer.EXPECT().
			Recv().
			Return(&rpc.ConnectRequest{
				Msg: &rpc.ConnectRequest_Descriptor_{
					Descriptor_: descriptor(),
				},
			}, nil),
		tunnelTracker.EXPECT().
			RegisterTunnel(gomock.Any(), gomock.Any(), gomock.Any()),
		tunnelTracker.EXPECT().
			UnregisterTunnel(gomock.Any(), gomock.Any()),
	)
	r := NewRegistry(testhelpers.NewLogger(t), mockAPI, nt(), time.Minute, time.Minute, time.Minute, tunnelTracker)
	var wg wait.Group
	defer wg.Wait()
	wg.Start(func() {
		err := r.Run(ctx)
		assert.NoError(t, err)
	})
	err := r.HandleTunnel(ctxConn, testhelpers.AgentInfoObj(), connectServer)
	assert.NoError(t, err)
	cancel()
}

// Two tunnels with the same agent id. Both register. Then one of them is retrieved via FindTunnel()
// and then its context is canceled. If this test gets stuck, we have a problem.
// Reproducer for https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/183.
func TestHandleTunnelIsUnblockedByContext_WithTwoTunnels(t *testing.T) {
	t.Parallel()
	ctrl := gomock.NewController(t)
	mockAPI := mock_modshared.NewMockAPI(ctrl)
	connectServer1 := mock_reverse_tunnel_rpc.NewMockReverseTunnel_ConnectServer(ctrl)
	connectServer2 := mock_reverse_tunnel_rpc.NewMockReverseTunnel_ConnectServer(ctrl)
	tunnelTracker := mock_agentk2kas_router.NewMockTracker(ctrl)
	connectServer1.EXPECT().
		Context().
		Return(context.Background()).
		MinTimes(1)
	connectServer2.EXPECT().
		Context().
		Return(context.Background()).
		MinTimes(1)
	d1 := descriptor()
	connectServer1.EXPECT().
		Recv().
		Return(&rpc.ConnectRequest{
			Msg: &rpc.ConnectRequest_Descriptor_{
				Descriptor_: d1,
			},
		}, nil)
	connectServer2.EXPECT().
		Recv().
		Return(&rpc.ConnectRequest{
			Msg: &rpc.ConnectRequest_Descriptor_{
				Descriptor_: descriptor(),
			},
		}, nil)
	gomock.InOrder(
		tunnelTracker.EXPECT().
			RegisterTunnel(gomock.Any(), gomock.Any(), gomock.Any()),
		tunnelTracker.EXPECT().
			UnregisterTunnel(gomock.Any(), gomock.Any()),
	)
	r := NewRegistry(testhelpers.NewLogger(t), mockAPI, nt(), time.Minute, time.Minute, time.Minute, tunnelTracker)
	var wgReg wait.Group
	defer wgReg.Wait()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	wgReg.Start(func() {
		err := r.Run(ctx)
		assert.NoError(t, err)
	})
	var wg wait.Group
	defer wg.Wait()
	agentInfo := testhelpers.AgentInfoObj()
	ctx1, cancel1 := context.WithCancel(context.Background())
	defer cancel1()
	wg.Start(func() {
		assert.NoError(t, r.HandleTunnel(ctx1, agentInfo, connectServer1))
	})
	ctx2, cancel2 := context.WithCancel(context.Background())
	defer cancel2()
	wg.Start(func() {
		assert.NoError(t, r.HandleTunnel(ctx2, agentInfo, connectServer2))
	})
	// wait for both to register
	assert.Eventually(t, func() bool {
		r.mu.Lock()
		defer r.mu.Unlock()
		return len(r.tunsByAgentID[agentInfo.ID].tuns) == 2
	}, time.Second, 10*time.Millisecond)
	found, th := r.FindTunnel(context.Background(), agentInfo.ID, serviceName, methodName)
	assert.True(t, found)
	tun, err := th.Get(context.Background())
	require.NoError(t, err)
	// cancel context for the found tunnel
	switch tun.(*tunserver.TunnelImpl).Tunnel {
	case connectServer1:
		cancel1()
	case connectServer2:
		cancel2()
	default:
		t.FailNow()
	}
	assert.Eventually(t, func() bool {
		r.mu.Lock()
		defer r.mu.Unlock()
		return tun.(*tunserver.TunnelImpl).State == tunserver.StateContextDone
	}, time.Second, 10*time.Millisecond)
	tun.Done(context.Background())
	th.Done(context.Background())
}

func TestHandleTunnelReturnErrOnRecvErr(t *testing.T) {
	ctrl := gomock.NewController(t)
	mockAPI := mock_modshared.NewMockAPI(ctrl)
	connectServer := mock_reverse_tunnel_rpc.NewMockReverseTunnel_ConnectServer(ctrl)
	connectServer.EXPECT().
		Context().
		Return(context.Background()).
		MinTimes(1)
	connectServer.EXPECT().
		Recv().
		Return(nil, status.Error(codes.DataLoss, "expected err"))
	tunnelTracker := mock_agentk2kas_router.NewMockTracker(ctrl)
	r := NewRegistry(testhelpers.NewLogger(t), mockAPI, nt(), time.Minute, time.Minute, time.Minute, tunnelTracker)
	err := r.HandleTunnel(context.Background(), testhelpers.AgentInfoObj(), connectServer)
	assert.EqualError(t, err, "rpc error: code = DataLoss desc = expected err")
}

func TestHandleTunnelReturnErrOnInvalidMsg(t *testing.T) {
	ctrl := gomock.NewController(t)
	mockAPI := mock_modshared.NewMockAPI(ctrl)
	connectServer := mock_reverse_tunnel_rpc.NewMockReverseTunnel_ConnectServer(ctrl)
	connectServer.EXPECT().
		Context().
		Return(context.Background()).
		MinTimes(1)
	connectServer.EXPECT().
		Recv().
		Return(&rpc.ConnectRequest{
			Msg: &rpc.ConnectRequest_Header{
				Header: &rpc.Header{},
			},
		}, nil)
	tunnelTracker := mock_agentk2kas_router.NewMockTracker(ctrl)
	r := NewRegistry(testhelpers.NewLogger(t), mockAPI, nt(), time.Minute, time.Minute, time.Minute, tunnelTracker)
	err := r.HandleTunnel(context.Background(), testhelpers.AgentInfoObj(), connectServer)
	assert.EqualError(t, err, "rpc error: code = InvalidArgument desc = invalid oneof value type: *rpc.ConnectRequest_Header")
}

func TestHandleTunnelIsMatchedToIncomingConnection(t *testing.T) {
	t.Parallel()
	incomingStream, rpcAPI, cb, tunnel, r := setupStreams(t, true)
	agentInfo := testhelpers.AgentInfoObj()
	var wg wait.Group
	defer wg.Wait()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	wg.Start(func() {
		err := r.Run(ctx)
		assert.NoError(t, err)
	})
	wg.Start(func() {
		assert.NoError(t, r.HandleTunnel(context.Background(), agentInfo, tunnel))
	})
	time.Sleep(50 * time.Millisecond)
	found, th := r.FindTunnel(context.Background(), agentInfo.ID, serviceName, methodName)
	defer th.Done(context.Background())
	assert.True(t, found)
	tun, err := th.Get(context.Background())
	require.NoError(t, err)
	defer tun.Done(context.Background())
	err = tun.ForwardStream(testhelpers.NewLogger(t), rpcAPI, incomingStream, cb)
	require.NoError(t, err)
}

func TestHandleTunnelIsNotMatchedToIncomingConnectionForMissingMethod(t *testing.T) {
	t.Parallel()
	ctrl := gomock.NewController(t)
	mockAPI := mock_modshared.NewMockAPI(ctrl)
	tunnelTracker := mock_agentk2kas_router.NewMockTracker(ctrl)
	connectServer := mock_reverse_tunnel_rpc.NewMockReverseTunnel_ConnectServer(ctrl)
	connectServer.EXPECT().
		Context().
		Return(context.Background()).
		MinTimes(1)
	connectServer.EXPECT().
		Recv().
		Return(&rpc.ConnectRequest{
			Msg: &rpc.ConnectRequest_Descriptor_{
				Descriptor_: descriptor(),
			},
		}, nil)
	gomock.InOrder(
		tunnelTracker.EXPECT().
			RegisterTunnel(gomock.Any(), gomock.Any(), gomock.Any()),
		tunnelTracker.EXPECT().
			UnregisterTunnel(gomock.Any(), gomock.Any()),
	)
	mockAPI.EXPECT().
		HandleProcessingError(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any())
	r := NewRegistry(testhelpers.NewLogger(t), mockAPI, nt(), time.Minute, time.Minute, time.Minute, tunnelTracker)
	agentInfo := testhelpers.AgentInfoObj()
	var wg wait.Group
	defer wg.Wait()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	wg.Start(func() {
		err := r.Run(ctx)
		assert.NoError(t, err)
	})
	wg.Start(func() {
		assert.NoError(t, r.HandleTunnel(context.Background(), agentInfo, connectServer))
	})
	time.Sleep(50 * time.Millisecond)
	ctx2, cancel2 := context.WithTimeout(context.Background(), 50*time.Millisecond)
	defer cancel2()
	found, th := r.FindTunnel(context.Background(), agentInfo.ID, "missing_service", "missing_method")
	defer th.Done(context.Background())
	assert.False(t, found)
	_, err := th.Get(ctx2)
	assert.Equal(t, context.DeadlineExceeded, err)
}

func TestForwardStreamIsMatchedToHandleTunnel(t *testing.T) {
	t.Parallel()
	incomingStream, rpcAPI, cb, tunnel, r := setupStreams(t, false)
	agentInfo := testhelpers.AgentInfoObj()
	var wg wait.Group
	defer wg.Wait()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	wg.Start(func() {
		err := r.Run(ctx)
		assert.NoError(t, err)
	})
	wg.Start(func() {
		_, th := r.FindTunnel(context.Background(), agentInfo.ID, serviceName, methodName)
		defer th.Done(context.Background())
		tun, err := th.Get(context.Background())
		if !assert.NoError(t, err) {
			return
		}
		defer tun.Done(context.Background())
		err = tun.ForwardStream(testhelpers.NewLogger(t), rpcAPI, incomingStream, cb)
		assert.NoError(t, err)
	})
	time.Sleep(50 * time.Millisecond)
	err := r.HandleTunnel(context.Background(), agentInfo, tunnel)
	require.NoError(t, err)
}

func TestForwardStreamIsNotMatchedToHandleTunnelForMissingMethod(t *testing.T) {
	t.Parallel()
	ctrl := gomock.NewController(t)
	mockAPI := mock_modshared.NewMockAPI(ctrl)
	tunnelTracker := mock_agentk2kas_router.NewMockTracker(ctrl)
	connectServer := mock_reverse_tunnel_rpc.NewMockReverseTunnel_ConnectServer(ctrl)
	connectServer.EXPECT().
		Context().
		Return(context.Background()).
		MinTimes(1)
	connectServer.EXPECT().
		Recv().
		Return(&rpc.ConnectRequest{
			Msg: &rpc.ConnectRequest_Descriptor_{
				Descriptor_: descriptor(),
			},
		}, nil)
	gomock.InOrder(
		tunnelTracker.EXPECT().
			RegisterTunnel(gomock.Any(), gomock.Any(), gomock.Any()),
		tunnelTracker.EXPECT().
			UnregisterTunnel(gomock.Any(), gomock.Any()),
	)
	mockAPI.EXPECT().
		HandleProcessingError(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any())
	r := NewRegistry(testhelpers.NewLogger(t), mockAPI, nt(), time.Minute, time.Minute, time.Minute, tunnelTracker)
	agentInfo := testhelpers.AgentInfoObj()
	var wg wait.Group
	defer wg.Wait()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	wg.Start(func() {
		err := r.Run(ctx)
		assert.NoError(t, err)
	})
	wg.Start(func() {
		found, th := r.FindTunnel(context.Background(), agentInfo.ID, "missing_service", "missing_method")
		defer th.Done(context.Background())
		assert.False(t, found)
		_, findErr := th.Get(context.Background())
		assert.EqualError(t, findErr, "rpc error: code = Canceled desc = kas is shutting down")
	})
	time.Sleep(50 * time.Millisecond)
	ctx2, cancel2 := context.WithTimeout(context.Background(), 50*time.Millisecond)
	defer cancel2()
	err := r.HandleTunnel(ctx2, agentInfo, connectServer)
	assert.NoError(t, err)
}

func TestFindTunnelIsUnblockedByContext(t *testing.T) {
	t.Parallel()
	ctxConn, cancelConn := context.WithTimeout(context.Background(), 50*time.Millisecond) // will unblock FindTunnel()
	defer cancelConn()

	ctrl := gomock.NewController(t)
	mockAPI := mock_modshared.NewMockAPI(ctrl)
	tunnelTracker := mock_agentk2kas_router.NewMockTracker(ctrl)
	r := NewRegistry(testhelpers.NewLogger(t), mockAPI, nt(), time.Minute, time.Minute, time.Minute, tunnelTracker)
	found, th := r.FindTunnel(context.Background(), testhelpers.AgentID, serviceName, methodName)
	defer th.Done(context.Background())
	assert.False(t, found)
	_, err := th.Get(ctxConn)
	assert.Equal(t, context.DeadlineExceeded, err)
}

func TestGC(t *testing.T) {
	t.Parallel()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	ctrl := gomock.NewController(t)
	mockAPI := mock_modserver.NewMockAPI(ctrl)
	tunnelTracker := mock_agentk2kas_router.NewMockTracker(ctrl)
	tunnelTracker.EXPECT().
		GC(gomock.Any(), gomock.Any()).
		DoAndReturn(func(ctx context.Context, int64s []int64) (int, error) {
			cancel()
			return 3, nil
		})
	r := NewRegistry(testhelpers.NewLogger(t), mockAPI, nt(), time.Minute, 50*time.Millisecond, time.Minute, tunnelTracker)

	err := r.Run(ctx)
	require.NoError(t, err)
}

func TestRefreshRegistrations(t *testing.T) {
	t.Parallel()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	ctrl := gomock.NewController(t)
	mockAPI := mock_modshared.NewMockAPI(ctrl)
	tunnelTracker := mock_agentk2kas_router.NewMockTracker(ctrl)
	r := NewRegistry(testhelpers.NewLogger(t), mockAPI, nt(), time.Minute, time.Minute, time.Minute, tunnelTracker)
	tunnelTracker.EXPECT().
		Refresh(gomock.Any(), gomock.Any(), gomock.Any()).
		Do(func(ctx context.Context, duration time.Duration, int64s []int64) error {
			cancel()
			return nil
		})

	r.refreshRegistrations()

	err := r.Run(ctx)
	require.NoError(t, err)
}

func setupStreams(t *testing.T, expectRegisterTunnel bool) (*mock_rpc.MockServerStream, *mock_modshared.MockRPCAPI, *mock_tunnel_tunserver.MockDataCallback, *mock_reverse_tunnel_rpc.MockReverseTunnel_ConnectServer, *Registry) {
	const metaKey = "Cba"
	meta := metadata.MD{}
	meta.Set(metaKey, "3", "4")
	ctrl := gomock.NewController(t)
	mockAPI := mock_modshared.NewMockAPI(ctrl)
	sts := mock_rpc.NewMockServerTransportStream(ctrl)
	cb := mock_tunnel_tunserver.NewMockDataCallback(ctrl)

	rpcAPI := mock_modshared.NewMockRPCAPI(ctrl)
	incomingCtx := grpc.NewContextWithServerTransportStream(context.Background(), sts)
	incomingCtx = metadata.NewIncomingContext(incomingCtx, meta)
	incomingStream := mock_rpc.NewMockServerStream(ctrl)
	incomingStream.EXPECT().
		Context().
		Return(incomingCtx).
		MinTimes(1)

	tunnelTracker := mock_agentk2kas_router.NewMockTracker(ctrl)
	connectServer := mock_reverse_tunnel_rpc.NewMockReverseTunnel_ConnectServer(ctrl)
	connectServer.EXPECT().
		Context().
		Return(context.Background()).
		MinTimes(1)
	connectServer.EXPECT().
		Recv().
		Return(&rpc.ConnectRequest{
			Msg: &rpc.ConnectRequest_Descriptor_{
				Descriptor_: descriptor(),
			},
		}, nil)
	if expectRegisterTunnel {
		gomock.InOrder(
			tunnelTracker.EXPECT().
				RegisterTunnel(gomock.Any(), gomock.Any(), gomock.Any()),
			tunnelTracker.EXPECT().
				UnregisterTunnel(gomock.Any(), gomock.Any()),
		)
	}
	frame := grpctool.RawFrame{
		Data: []byte{1, 2, 3},
	}
	gomock.InOrder(
		sts.EXPECT().
			Method().
			Return(fullMethodName).
			MinTimes(1),
		connectServer.EXPECT().
			Send(matcher.ProtoEq(t, &rpc.ConnectResponse{
				Msg: &rpc.ConnectResponse_RequestInfo{
					RequestInfo: &rpc.RequestInfo{
						MethodName: fullMethodName,
						Meta: map[string]*prototool.HeaderValues{
							"cba": {Value: []string{"3", "4"}},
						},
					},
				},
			})),
		incomingStream.EXPECT().
			RecvMsg(gomock.Any()).
			Do(testhelpers.RecvMsg(&frame)),
		connectServer.EXPECT().
			Send(matcher.ProtoEq(t, &rpc.ConnectResponse{
				Msg: &rpc.ConnectResponse_Message{
					Message: &rpc.Message{
						Data: frame.Data,
					},
				},
			})),
		incomingStream.EXPECT().
			RecvMsg(gomock.Any()).
			Return(io.EOF),
		connectServer.EXPECT().
			Send(matcher.ProtoEq(t, &rpc.ConnectResponse{
				Msg: &rpc.ConnectResponse_CloseSend{
					CloseSend: &rpc.CloseSend{},
				},
			})),
	)
	gomock.InOrder(
		connectServer.EXPECT().
			RecvMsg(gomock.Any()).
			Do(testhelpers.RecvMsg(&rpc.ConnectRequest{
				Msg: &rpc.ConnectRequest_Header{
					Header: &rpc.Header{
						Meta: map[string]*prototool.HeaderValues{
							"resp": {Value: []string{"1", "2"}},
						},
					},
				},
			})),
		cb.EXPECT().
			Header(map[string]*prototool.HeaderValues{
				"resp": {Value: []string{"1", "2"}},
			}),
		connectServer.EXPECT().
			RecvMsg(gomock.Any()).
			Do(testhelpers.RecvMsg(&rpc.ConnectRequest{
				Msg: &rpc.ConnectRequest_Message{
					Message: &rpc.Message{
						Data: []byte{5, 6, 7},
					},
				},
			})),
		cb.EXPECT().
			Message([]byte{5, 6, 7}),
		connectServer.EXPECT().
			RecvMsg(gomock.Any()).
			Do(testhelpers.RecvMsg(&rpc.ConnectRequest{
				Msg: &rpc.ConnectRequest_Trailer{
					Trailer: &rpc.Trailer{
						Meta: map[string]*prototool.HeaderValues{
							"trailer": {Value: []string{"8", "9"}},
						},
					},
				},
			})),
		cb.EXPECT().
			Trailer(map[string]*prototool.HeaderValues{
				"trailer": {Value: []string{"8", "9"}},
			}),
		connectServer.EXPECT().
			RecvMsg(gomock.Any()).
			Return(io.EOF),
	)

	r := NewRegistry(testhelpers.NewLogger(t), mockAPI, nt(), time.Minute, time.Minute, time.Minute, tunnelTracker)
	return incomingStream, rpcAPI, cb, connectServer, r
}

func descriptor() *rpc.Descriptor {
	return &rpc.Descriptor{
		ApiDescriptor: &info.APIDescriptor{
			Services: []*info.Service{
				{
					Name: serviceName,
					Methods: []*info.Method{
						{
							Name: methodName,
						},
					},
				},
			},
		},
	}
}

func nt() trace.Tracer {
	return noop.NewTracerProvider().Tracer("test")
}
