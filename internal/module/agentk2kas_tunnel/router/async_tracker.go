package router

import (
	"context"
	"fmt"
	"log/slog"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/fieldz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
)

type asyncTaskType byte

const (
	_ asyncTaskType = iota
	asyncTaskRegister
	asyncTaskUnregister
	asyncTaskGC
	asyncTaskRefresh
)

const (
	queueSize = 256
)

type asyncTask struct {
	taskType asyncTaskType
	agentIDs []int64
}

// asyncTracker performs all tunnel tracker (un)registration/refresh/GC I/O for the registry.
// By using a single chanel to submit work we ensure that no races is possible and e.g. a tunnel registration and
// unregistration cannot be reordered.
// Code in Registry that uses asyncTracker must always hold a mutex while both checking state and using
// asyncTracker's methods. This ensures there are no races in the client code i.e. requests to perform I/O
// are submitted to the asyncTracker without any reordering due to concurrency.
type asyncTracker struct {
	log           *slog.Logger
	api           modshared.API
	tunnelTracker Registerer
	ttl           time.Duration
	taskQueue     chan asyncTask
}

func newAsyncTracker(log *slog.Logger, api modshared.API, tunnelTracker Registerer, ttl time.Duration) *asyncTracker {
	return &asyncTracker{
		log:           log,
		api:           api,
		tunnelTracker: tunnelTracker,
		ttl:           ttl,
		taskQueue:     make(chan asyncTask, queueSize),
	}
}

func (t *asyncTracker) done() {
	close(t.taskQueue)
}

func (t *asyncTracker) run() {
	for task := range t.taskQueue {
		switch task.taskType {
		case asyncTaskRegister:
			for _, agentID := range task.agentIDs {
				err := t.tunnelTracker.RegisterTunnel(context.Background(), t.ttl, agentID)
				if err != nil {
					t.api.HandleProcessingError(context.Background(), t.log.With(logz.AgentID(agentID)),
						"Failed to register tunnel", err, fieldz.AgentID(agentID))
				}
			}
		case asyncTaskUnregister:
			for _, agentID := range task.agentIDs {
				err := t.tunnelTracker.UnregisterTunnel(context.Background(), agentID)
				if err != nil {
					t.api.HandleProcessingError(context.Background(), t.log.With(logz.AgentID(agentID)),
						"Failed to unregister tunnel", err, fieldz.AgentID(agentID))
				}
			}
		case asyncTaskRefresh:
			err := t.tunnelTracker.Refresh(context.Background(), t.ttl, task.agentIDs)
			if err != nil {
				t.api.HandleProcessingError(context.Background(), t.log, "Failed to refresh data", err)
			}
		case asyncTaskGC:
			deletedKeys, err := t.tunnelTracker.GC(context.Background(), task.agentIDs)
			if err != nil {
				t.api.HandleProcessingError(context.Background(), t.log, "Failed to GC data of registry stripe", err)
			}
			if deletedKeys > 0 {
				t.log.Info("Deleted expired agent tunnel records", logz.RemovedHashKeys(deletedKeys))
			}
		default:
			panic(fmt.Sprintf("unknown task type: %d", task.taskType))
		}
	}
}

// RegisterTunnel registers tunnel with the tracker.
func (t *asyncTracker) registerTunnel(agentID int64) {
	t.taskQueue <- asyncTask{
		taskType: asyncTaskRegister,
		agentIDs: []int64{agentID},
	}
}

// UnregisterTunnel unregisters tunnel with the tracker.
func (t *asyncTracker) unregisterTunnel(agentID int64) {
	t.taskQueue <- asyncTask{
		taskType: asyncTaskUnregister,
		agentIDs: []int64{agentID},
	}
}

// GC deletes expired tunnels from the underlying storage.
func (t *asyncTracker) gc(agentIDs []int64) {
	t.taskQueue <- asyncTask{
		taskType: asyncTaskGC,
		agentIDs: agentIDs,
	}
}

// Refresh refreshes registered tunnels in the underlying storage.
func (t *asyncTracker) refresh(agentIDs []int64) {
	t.taskQueue <- asyncTask{
		taskType: asyncTaskRefresh,
		agentIDs: agentIDs,
	}
}
