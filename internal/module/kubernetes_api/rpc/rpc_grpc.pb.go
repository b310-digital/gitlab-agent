// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.5.1
// - protoc             v5.27.1
// source: internal/module/kubernetes_api/rpc/rpc.proto

package rpc

import (
	context "context"

	grpctool "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.64.0 or later.
const _ = grpc.SupportPackageIsVersion9

const (
	KubernetesApi_MakeRequest_FullMethodName = "/gitlab.agent.kubernetes_api.rpc.KubernetesApi/MakeRequest"
)

// KubernetesApiClient is the client API for KubernetesApi service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type KubernetesApiClient interface {
	MakeRequest(ctx context.Context, opts ...grpc.CallOption) (grpc.BidiStreamingClient[grpctool.HttpRequest, grpctool.HttpResponse], error)
}

type kubernetesApiClient struct {
	cc grpc.ClientConnInterface
}

func NewKubernetesApiClient(cc grpc.ClientConnInterface) KubernetesApiClient {
	return &kubernetesApiClient{cc}
}

func (c *kubernetesApiClient) MakeRequest(ctx context.Context, opts ...grpc.CallOption) (grpc.BidiStreamingClient[grpctool.HttpRequest, grpctool.HttpResponse], error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	stream, err := c.cc.NewStream(ctx, &KubernetesApi_ServiceDesc.Streams[0], KubernetesApi_MakeRequest_FullMethodName, cOpts...)
	if err != nil {
		return nil, err
	}
	x := &grpc.GenericClientStream[grpctool.HttpRequest, grpctool.HttpResponse]{ClientStream: stream}
	return x, nil
}

// This type alias is provided for backwards compatibility with existing code that references the prior non-generic stream type by name.
type KubernetesApi_MakeRequestClient = grpc.BidiStreamingClient[grpctool.HttpRequest, grpctool.HttpResponse]

// KubernetesApiServer is the server API for KubernetesApi service.
// All implementations must embed UnimplementedKubernetesApiServer
// for forward compatibility.
type KubernetesApiServer interface {
	MakeRequest(grpc.BidiStreamingServer[grpctool.HttpRequest, grpctool.HttpResponse]) error
	mustEmbedUnimplementedKubernetesApiServer()
}

// UnimplementedKubernetesApiServer must be embedded to have
// forward compatible implementations.
//
// NOTE: this should be embedded by value instead of pointer to avoid a nil
// pointer dereference when methods are called.
type UnimplementedKubernetesApiServer struct{}

func (UnimplementedKubernetesApiServer) MakeRequest(grpc.BidiStreamingServer[grpctool.HttpRequest, grpctool.HttpResponse]) error {
	return status.Errorf(codes.Unimplemented, "method MakeRequest not implemented")
}
func (UnimplementedKubernetesApiServer) mustEmbedUnimplementedKubernetesApiServer() {}
func (UnimplementedKubernetesApiServer) testEmbeddedByValue()                       {}

// UnsafeKubernetesApiServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to KubernetesApiServer will
// result in compilation errors.
type UnsafeKubernetesApiServer interface {
	mustEmbedUnimplementedKubernetesApiServer()
}

func RegisterKubernetesApiServer(s grpc.ServiceRegistrar, srv KubernetesApiServer) {
	// If the following call pancis, it indicates UnimplementedKubernetesApiServer was
	// embedded by pointer and is nil.  This will cause panics if an
	// unimplemented method is ever invoked, so we test this at initialization
	// time to prevent it from happening at runtime later due to I/O.
	if t, ok := srv.(interface{ testEmbeddedByValue() }); ok {
		t.testEmbeddedByValue()
	}
	s.RegisterService(&KubernetesApi_ServiceDesc, srv)
}

func _KubernetesApi_MakeRequest_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(KubernetesApiServer).MakeRequest(&grpc.GenericServerStream[grpctool.HttpRequest, grpctool.HttpResponse]{ServerStream: stream})
}

// This type alias is provided for backwards compatibility with existing code that references the prior non-generic stream type by name.
type KubernetesApi_MakeRequestServer = grpc.BidiStreamingServer[grpctool.HttpRequest, grpctool.HttpResponse]

// KubernetesApi_ServiceDesc is the grpc.ServiceDesc for KubernetesApi service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var KubernetesApi_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "gitlab.agent.kubernetes_api.rpc.KubernetesApi",
	HandlerType: (*KubernetesApiServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "MakeRequest",
			Handler:       _KubernetesApi_MakeRequest_Handler,
			ServerStreams: true,
			ClientStreams: true,
		},
	},
	Metadata: "internal/module/kubernetes_api/rpc/rpc.proto",
}
