package watch_aggregator //nolint:stylecheck

import (
	"io"
	"net/http"
)

var (
	_ http.ResponseWriter = (*httpResponseWriterToWebsocketWriter)(nil)
	_ http.Flusher        = (*httpResponseWriterToWebsocketWriter)(nil)
)

// httpResponseWriterToWebsocketWriter represents an object to bridge
// the proxy function with the event stream decoder.
type httpResponseWriterToWebsocketWriter struct {
	writer      io.Writer
	writeHeader func(int, http.Header)
	header      http.Header
}

func newHTTPResponseWriterToWebsocketWriter(w io.Writer, writeHeader func(int, http.Header)) *httpResponseWriterToWebsocketWriter {
	return &httpResponseWriterToWebsocketWriter{writer: w, writeHeader: writeHeader, header: http.Header{}}
}

func (w *httpResponseWriterToWebsocketWriter) Header() http.Header {
	return w.header
}

func (w *httpResponseWriterToWebsocketWriter) Write(r []byte) (int, error) {
	return w.writer.Write(r)
}

func (w *httpResponseWriterToWebsocketWriter) WriteHeader(statusCode int) {
	w.writeHeader(statusCode, w.header)
}

func (w *httpResponseWriterToWebsocketWriter) Flush() {}
