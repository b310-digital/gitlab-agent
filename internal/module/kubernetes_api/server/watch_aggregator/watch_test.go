package watch_aggregator //nolint:stylecheck

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/httpz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
	"go.uber.org/mock/gomock"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestWatch_SendsWatchEvent(t *testing.T) {
	// GIVEN
	ctrl := gomock.NewController(t)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	watchID := "any-watch-id"
	mockSender := NewMockwatchResponseSender(ctrl)
	event := k8sWatchEvent{Type: "ADDED", Object: &corev1.Pod{TypeMeta: metav1.TypeMeta{Kind: "pod", APIVersion: "v1"}}}
	proxyRequest := func(w http.ResponseWriter, r *http.Request) (*grpctool.ErrResp, bool) {
		w.Header().Add(httpz.ContentTypeHeader, acceptedContentType)
		w.WriteHeader(http.StatusOK)

		b, err := json.Marshal(&event)
		require.NoError(t, err) //nolint:testifylint
		_, err = w.Write(b)
		require.NoError(t, err) //nolint:testifylint

		return nil, false
	}
	watch, err := newWatch(ctx, testhelpers.NewLogger(t), proxyRequest, &WatchRequest{WatchId: watchID, Type: "watch", WatchParams: &WatchParams{}}, mockSender)
	require.NoError(t, err)

	// THEN
	gomock.InOrder(
		mockSender.EXPECT().sendEvent(watchID, gomock.Any()),
		mockSender.EXPECT().sendStop(watchID),
	)

	// WHEN
	watch.handle()
}

func TestWatch_WhenUnableToCreateDownstreamWatch(t *testing.T) {
	// GIVEN
	ctrl := gomock.NewController(t)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	watchID := "any-watch-id"
	mockSender := NewMockwatchResponseSender(ctrl)
	event := &metav1.Status{TypeMeta: metav1.TypeMeta{Kind: "status", APIVersion: "v1"}, Status: "Failure", Message: "something went wrong"}
	proxyRequest := func(w http.ResponseWriter, r *http.Request) (*grpctool.ErrResp, bool) {
		w.Header().Add(httpz.ContentTypeHeader, acceptedContentType)
		w.WriteHeader(http.StatusBadRequest)

		b, err := json.Marshal(&event)
		require.NoError(t, err) //nolint:testifylint
		_, err = w.Write(b)
		require.NoError(t, err) //nolint:testifylint

		return nil, false
	}
	watch, err := newWatch(ctx, testhelpers.NewLogger(t), proxyRequest, &WatchRequest{WatchId: watchID, Type: "watch", WatchParams: &WatchParams{}}, mockSender)
	require.NoError(t, err)

	// THEN
	mockSender.EXPECT().sendError(watchID, watchRequestFailedErrorType, fmt.Errorf("unable to watch"), gomock.Any())

	// WHEN
	watch.handle()
}
