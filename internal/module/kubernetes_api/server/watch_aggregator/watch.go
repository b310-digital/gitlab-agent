package watch_aggregator //nolint:stylecheck

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"net/url"
	"path"
	"strconv"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/httpz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/ioz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"k8s.io/apimachinery/pkg/util/wait"
	restclientwatch "k8s.io/client-go/rest/watch"
)

var (
	_ watchFactory   = (*basicWatchFactory)(nil)
	_ watchInterface = (*watch)(nil)
)

const (
	acceptedContentType = "application/json"
)

type basicWatchFactory struct {
}

func (f *basicWatchFactory) new(ctx context.Context, log *slog.Logger, proxyRequest ProxyRequest, watchRequest *WatchRequest, sender watchResponseSender) (watchInterface, error) {
	return newWatch(ctx, log, proxyRequest, watchRequest, sender)
}

type watch struct {
	id              string
	sender          watchResponseSender
	decoder         *restclientwatch.Decoder
	errorDecoder    *watchErrorDecoder
	discardResponse func()
	receiveHeaders  func() (int /* status code */, http.Header, bool)
	proxyFunc       func()
}

type errWatchFailed struct {
	inner *grpctool.ErrResp
}

func (e *errWatchFailed) Error() string {
	if e.inner.Err == nil {
		return fmt.Sprintf("watch failed with %s (status code=%d)", e.inner.Msg, e.inner.StatusCode)
	}
	return fmt.Sprintf("watch failed with %s (status code=%d) and error %s", e.inner.Msg, e.inner.StatusCode, e.inner.Err.Error())
}

var errWatchAborted = errors.New("watch proxy request aborted")

type writtenHeader struct {
	statusCode int
	header     http.Header
}

// newWatch constructs a new watch object
// It sets up the following:
// - a new watch request from the given watchRequest to proxy to a Kube API server.
// - a proxy HTTP response writer
// - a proxy function with the request and the response writer.
// - a watch event frame decoder to decode the watch HTTP response stream into watch event objects.
// - an in-memory pipe to connect the proxy response writer with the event frame decoder.
// A watch can only be started a single time. Don't restart it.
func newWatch(ctx context.Context, log *slog.Logger, proxyRequest ProxyRequest, watchRequest *WatchRequest, sender watchResponseSender) (*watch, error) {
	if watchRequest.WatchParams == nil {
		return nil, errors.New("missing watch params definition in watch event")
	}

	r, err := newWatchRequest(ctx, watchRequest.WatchParams)
	if err != nil {
		return nil, err
	}

	headerCh := make(chan writtenHeader)

	pr, pw := io.Pipe()
	w := &watch{
		id:           watchRequest.WatchId,
		sender:       sender,
		decoder:      newWatchDecoder(pr),
		errorDecoder: newWatchErrorDecoder(pr),
		receiveHeaders: func() (int /* status code */, http.Header, bool) {
			h, ok := <-headerCh
			if !ok {
				return 0, nil, false // shutdown
			}

			return h.statusCode, h.header, true
		},
		discardResponse: func() {
			if err := ioz.DiscardData(pr); err != nil {
				log.Debug("Unable to discard watch response body", logz.WatchID(watchRequest.WatchId), logz.Error(err))
			}
		},
		proxyFunc: func() {
			w := newHTTPResponseWriterToWebsocketWriter(pw, func(statusCode int, header http.Header) {
				select {
				case <-ctx.Done():
					close(headerCh)
				case headerCh <- writtenHeader{statusCode: statusCode, header: header}:
				}
			})
			eResp, abort := proxyRequest(w, r)

			// If we have an abort, but the watch ctx is canceled it indicates a normal shutdown flow
			// and we don't want to report an abort error.
			if abort && ctx.Err() == nil {
				_ = pw.CloseWithError(errWatchAborted)
				return
			}

			if eResp != nil {
				_ = pw.CloseWithError(&errWatchFailed{inner: eResp})
				return
			}

			_ = pw.Close()
		},
	}

	return w, nil
}

// handle the watch
// Starts a goroutine to receive data from the watch event stream
// and forward them to the websocket response channel.
// And starts the proxy request.
func (w *watch) handle() {
	var wg wait.Group
	defer wg.Wait()

	wg.Start(func() {
		statusCode, header, ok := w.receiveHeaders()
		if !ok {
			return // shutdown
		}

		if ct := header.Get(httpz.ContentTypeHeader); !httpz.IsContentType(ct, acceptedContentType) {
			w.sender.sendError(w.id, watchFailedErrorType, fmt.Errorf("got unexpected %q content-type in watch response with status code %d", ct, statusCode), nil)
			w.discardResponse()
			return
		}

		switch statusCode {
		case http.StatusOK:
			w.receiveWatchEvents()
		default:
			w.receiveErrorResponse()
		}
	})

	w.proxyFunc()
}

func (w *watch) receiveWatchEvents() {
	for {
		action, obj, err := w.decoder.Decode()
		if err != nil {

			var watchFailedErr *errWatchFailed
			if errors.As(err, &watchFailedErr) {
				w.sender.sendError(w.id, watchFailedErrorType, watchFailedErr, nil)
				return
			}

			switch err {
			case errWatchAborted: //nolint:errorlint
				w.sender.sendError(w.id, watchFailedErrorType, err, nil)
			case io.EOF:
				w.sender.sendStop(w.id)
			case io.ErrUnexpectedEOF:
				w.sender.sendError(w.id, watchFailedErrorType, fmt.Errorf("unexpected EOF during watch stream event decoding: %w", err), nil)
			default:
				w.sender.sendError(w.id, watchFailedErrorType, fmt.Errorf("unable to decode an event from the watch stream: %w", err), nil)
			}

			return
		}

		w.sender.sendEvent(w.id, &k8sWatchEvent{Type: string(action), Object: obj})
	}
}

func (w *watch) receiveErrorResponse() {
	o, err := w.errorDecoder.decode()
	if err != nil {
		w.sender.sendError(w.id, watchRequestFailedErrorType, fmt.Errorf("unable to decode watch error: %w", err), nil)
		return
	}

	w.sender.sendError(w.id, watchRequestFailedErrorType, fmt.Errorf("unable to watch"), o)
}

func newWatchRequest(ctx context.Context, params *WatchParams) (*http.Request, error) {
	u := url.URL{
		Path:     constructAPIPath(params),
		RawQuery: constructAPIQuery(params),
	}

	r, err := http.NewRequestWithContext(
		ctx,
		http.MethodGet,
		u.String(),
		http.NoBody,
	)
	if err != nil {
		return nil, err
	}
	r.Header[httpz.AcceptHeader] = []string{acceptedContentType}
	r.Header[httpz.ConnectionHeader] = []string{"close"}

	return r, nil
}

func constructAPIPath(params *WatchParams) string {
	var p []string
	if params.Group == "" {
		p = []string{"/api"}
	} else {
		p = []string{"/apis", url.PathEscape(params.Group)}
	}

	p = append(p, url.PathEscape(params.Version))

	if params.Namespace != "" {
		p = append(p, "namespaces", url.PathEscape(params.Namespace))
	}

	p = append(p, url.PathEscape(params.Resource))
	return path.Join(p...)
}

func constructAPIQuery(params *WatchParams) string {
	query := url.Values{"watch": []string{"true"}}
	if params.ResourceVersion != nil {
		query.Add("resourceVersion", *params.ResourceVersion)
	}
	if params.AllowWatchBookmarks != nil {
		query.Add("allowWatchBookmarks", strconv.FormatBool(*params.AllowWatchBookmarks))
	}
	if params.SendInitialEvents != nil {
		query.Add("sendInitialEvents", strconv.FormatBool(*params.SendInitialEvents))
	}
	if params.FieldSelector != nil {
		query.Add("fieldSelector", *params.FieldSelector)
	}
	if params.LabelSelector != nil {
		query.Add("labelSelector", *params.LabelSelector)
	}
	return query.Encode()
}
