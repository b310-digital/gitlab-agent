package agent

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_configuration"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_configuration/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modagent"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
)

type Factory struct {
}

func (f *Factory) IsProducingLeaderModules() bool {
	return false
}

func (f *Factory) Name() string {
	return agent_configuration.ModuleName
}

func (f *Factory) New(config *modagent.Config) (modagent.Module, error) {
	config.RegisterKASAPI(&rpc.AgentConfiguration_ServiceDesc)
	return nil, nil
}

func (f *Factory) StartStopPhase() modshared.ModuleStartStopPhase {
	return modshared.ModuleStartBeforeServers
}
