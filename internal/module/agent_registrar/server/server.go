package server

import (
	"context"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_registrar/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/event_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/fieldz"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type RegisterAgentEvent struct {
	ProjectID    int64  `json:"project_id"`
	AgentVersion string `json:"agent_version"`
	Architecture string `json:"architecture"`
	ConnectionID int64  `json:"-"`
}

func (e RegisterAgentEvent) DeduplicateKey() string {
	return strconv.FormatInt(e.ConnectionID, 10)
}

type server struct {
	rpc.UnimplementedAgentRegistrarServer
	agentRegisterer           agent_tracker.ExpiringRegisterer
	registerAgentEventTracker event_tracker.EventsInterface
}

func (s *server) Register(ctx context.Context, req *rpc.RegisterRequest) (*rpc.RegisterResponse, error) {
	rpcAPI := modserver.AgentRPCAPIFromContext(ctx)
	log := rpcAPI.Log()

	// Get agent info
	agentInfo, err := rpcAPI.AgentInfo(ctx, log)
	if err != nil {
		return nil, err
	}

	// If GitRef is not set, use CommitId for backward compatibility
	if req.AgentMeta.CommitId != "" && req.AgentMeta.GitRef == "" { //nolint:staticcheck
		req.AgentMeta.GitRef = req.AgentMeta.CommitId //nolint:staticcheck
	}

	connectedAgentInfo := &agent_tracker.ConnectedAgentInfo{
		AgentMeta:    req.AgentMeta,
		ConnectedAt:  timestamppb.Now(),
		ConnectionId: req.PodId,
		AgentId:      agentInfo.ID,
		ProjectId:    agentInfo.ProjectID,
	}

	// Register agent
	err = s.agentRegisterer.RegisterExpiring(ctx, connectedAgentInfo)
	if err != nil {
		rpcAPI.HandleProcessingError(log, "Failed to register agent", err, fieldz.AgentID(agentInfo.ID))
		return nil, status.Error(codes.Unavailable, "failed to register agent")
	}

	event := RegisterAgentEvent{
		ProjectID:    agentInfo.ProjectID,
		AgentVersion: req.AgentMeta.Version,
		Architecture: extractArch(req.AgentMeta.KubernetesVersion.Platform),
		ConnectionID: req.PodId,
	}
	s.registerAgentEventTracker.EmitEvent(event)
	return &rpc.RegisterResponse{}, nil
}

func (s *server) Unregister(ctx context.Context, req *rpc.UnregisterRequest) (*rpc.UnregisterResponse, error) {
	rpcAPI := modserver.AgentRPCAPIFromContext(ctx)
	log := rpcAPI.Log()

	// Get agent info
	agentInfo, err := rpcAPI.AgentInfo(ctx, log)
	if err != nil {
		return nil, err
	}

	disconnectAgentInfo := &agent_tracker.DisconnectAgentInfo{
		AgentMeta:    req.AgentMeta,
		ConnectionId: req.PodId,
		AgentId:      agentInfo.ID,
		ProjectId:    agentInfo.ProjectID,
	}

	// Register agent
	err = s.agentRegisterer.Unregister(ctx, disconnectAgentInfo)
	if err != nil {
		rpcAPI.HandleProcessingError(log, "Failed to unregister agent", err, fieldz.AgentID(agentInfo.ID))
		return nil, status.Error(codes.Unavailable, "failed to unregister agent")
	}

	return &rpc.UnregisterResponse{}, nil
}

func extractArch(platform string) string {
	_, arch, found := strings.Cut(platform, "/")
	if !found {
		return ""
	}
	return arch
}
