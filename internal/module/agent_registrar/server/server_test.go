package server

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_registrar/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/fieldz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_event_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
	"go.uber.org/mock/gomock"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	_ modserver.Factory = (*Factory)(nil)
)

func TestRegister(t *testing.T) {
	mockRPCAPI, mockAgentTracker, mockEventTracker, s, req, ctx := setupServer(t)

	mockRPCAPI.EXPECT().
		Log().
		Return(testhelpers.NewLogger(t))
	mockRPCAPI.EXPECT().
		AgentInfo(gomock.Any(), gomock.Any()).
		Return(&api.AgentInfo{ID: 123, ProjectID: 456}, nil)
	mockAgentTracker.EXPECT().
		RegisterExpiring(gomock.Any(), gomock.Any()).
		Do(func(ctx context.Context, connectedAgentInfo *agent_tracker.ConnectedAgentInfo) error {
			assert.EqualValues(t, 123, connectedAgentInfo.AgentId)
			assert.EqualValues(t, 456, connectedAgentInfo.ProjectId)
			assert.EqualValues(t, 123456789, connectedAgentInfo.ConnectionId)
			return nil
		})
	expectedEvent := RegisterAgentEvent{
		ProjectID:    456,
		AgentVersion: "v17.1.0",
		Architecture: "amd64",
		ConnectionID: 123456789,
	}
	mockEventTracker.EXPECT().EmitEvent(expectedEvent)

	resp, err := s.Register(ctx, req)
	assert.NotNil(t, resp)
	assert.NoError(t, err)
}

func TestRegister_AgentInfo_Error(t *testing.T) {
	mockRPCAPI, _, _, s, req, ctx := setupServer(t)

	mockRPCAPI.EXPECT().
		Log().
		Return(testhelpers.NewLogger(t))
	mockRPCAPI.EXPECT().
		AgentInfo(gomock.Any(), gomock.Any()).
		Return(nil, status.Error(codes.Unavailable, "failed to register agent"))

	resp, err := s.Register(ctx, req)
	assert.Nil(t, resp)
	assert.Equal(t, codes.Unavailable, status.Code(err))
}

func TestRegister_registerAgent_Error(t *testing.T) {
	mockRPCAPI, mockAgentTracker, _, s, req, ctx := setupServer(t)

	expectedErr := errors.New("expected error")

	mockRPCAPI.EXPECT().
		Log().
		Return(testhelpers.NewLogger(t))
	mockRPCAPI.EXPECT().
		AgentInfo(gomock.Any(), gomock.Any()).
		Return(&api.AgentInfo{ID: testhelpers.AgentID, ProjectID: testhelpers.ProjectID}, nil)
	mockAgentTracker.EXPECT().
		RegisterExpiring(gomock.Any(), gomock.Any()).
		Return(expectedErr)
	mockRPCAPI.EXPECT().
		HandleProcessingError(gomock.Any(), gomock.Any(), expectedErr, fieldz.AgentID(testhelpers.AgentID))

	resp, err := s.Register(ctx, req)
	assert.Nil(t, resp)
	assert.Equal(t, codes.Unavailable, status.Code(err))
}

func setupServer(t *testing.T) (*mock_modserver.MockAgentRPCAPI,
	*mock_agent_tracker.MockTracker, *mock_event_tracker.MockEventsInterface, *server, *rpc.RegisterRequest, context.Context) {
	ctrl := gomock.NewController(t)

	mockRPCAPI := mock_modserver.NewMockAgentRPCAPI(ctrl)
	mockAgentTracker := mock_agent_tracker.NewMockTracker(ctrl)
	mockEventTracker := mock_event_tracker.NewMockEventsInterface(ctrl)

	s := &server{
		agentRegisterer:           mockAgentTracker,
		registerAgentEventTracker: mockEventTracker,
	}

	req := &rpc.RegisterRequest{
		AgentMeta: mock_modserver.AgentMeta(),
		PodId:     123456789,
	}

	ctx := modserver.InjectAgentRPCAPI(context.Background(), mockRPCAPI)

	return mockRPCAPI, mockAgentTracker, mockEventTracker, s, req, ctx
}
