package agent

import (
	"context"
	"errors"
	"log/slog"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"k8s.io/apimachinery/pkg/api/equality"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/tools/cache"
)

// https://caiorcferreira.github.io/post/the-kubernetes-dynamic-client/

type k8sInformer struct {
	informer       cache.SharedIndexInformer
	log            *slog.Logger
	backgroundTask stoppableTask
}

func newK8sInformer(log *slog.Logger, informer cache.SharedIndexInformer) (*k8sInformer, error) {
	_, err := informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj any) {
			// Handler logic
			u := obj.(*unstructured.Unstructured)
			log.Debug("Received add event", extractEventFields(u)...)
		},
		UpdateFunc: func(oldObj, newObj any) {
			// Handler logic
			newU := newObj.(*unstructured.Unstructured)
			oldU := oldObj.(*unstructured.Unstructured)
			if equality.Semantic.DeepEqual(oldU, newU) {
				return
			}
			log.Debug("Received update event", extractEventFields(newU)...)
		},
		DeleteFunc: func(obj any) {
			// Handler logic
			switch u := obj.(type) {
			case *unstructured.Unstructured:
				log.Debug("Received delete event", extractEventFields(u)...)
			default:
				log.Debug("Received unknown delete event")
			}
		},
	})
	if err != nil {
		return nil, err
	}
	return &k8sInformer{
		informer: informer,
		log:      log,
	}, nil
}

func extractEventFields(event *unstructured.Unstructured) []any {
	return []any{
		logz.WorkspaceNamespace(event.GetNamespace()),
		logz.WorkspaceName(event.GetName()),
	}
}

func (i *k8sInformer) Start(ctx context.Context) error {
	i.backgroundTask = newStoppableTask(ctx, func(ctx context.Context) {
		i.informer.Run(ctx.Done())
	})

	isSynced := cache.WaitForCacheSync(ctx.Done(), i.informer.HasSynced)

	if !isSynced {
		return errors.New("failed to sync informer during init")
	}

	return nil
}

func (i *k8sInformer) List() []*parsedWorkspace {
	list := i.informer.GetIndexer().List()
	result := make([]*parsedWorkspace, 0, len(list))

	for _, raw := range list {
		result = append(result, i.parseUnstructuredToWorkspace(raw.(*unstructured.Unstructured)))
	}

	return result
}

func (i *k8sInformer) parseUnstructuredToWorkspace(rawWorkspace *unstructured.Unstructured) *parsedWorkspace {
	return &parsedWorkspace{
		Name:              rawWorkspace.GetName(),
		Namespace:         rawWorkspace.GetNamespace(),
		ResourceVersion:   rawWorkspace.GetResourceVersion(),
		K8sDeploymentInfo: rawWorkspace.Object,
	}
}

func (i *k8sInformer) Stop() {
	if i.backgroundTask != nil {
		i.backgroundTask.StopAndWait()
		i.log.Info("informer stopped")
	}
}
