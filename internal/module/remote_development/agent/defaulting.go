package agent

import "time"

const (
	// default full reconciliation interval is 1 hour
	defaultFullReconciliationInterval = 1 * time.Hour
	// default partial reconciliation interval is 10 seconds
	defaultPartialReconciliationInterval = 10 * time.Second
)

// We provide a default response payload to handle the absence of field values from API
func newResponsePayloadWithDefaultSettings() *ResponsePayload {
	return &ResponsePayload{
		Settings: newDefaultSettings(),
	}
}

func newDefaultSettings() *Settings {
	return &Settings{
		FullReconciliationIntervalSeconds:    int(defaultFullReconciliationInterval.Seconds()),
		PartialReconciliationIntervalSeconds: int(defaultPartialReconciliationInterval.Seconds()),
	}
}
