package server

import (
	"context"
	"log/slog"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab"
	gapi "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/kas2agentk_tunnel"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/retry"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/syncz"
)

type module struct {
	log                   *slog.Logger
	api                   modserver.API
	gitLabClient          gitlab.ClientInterface
	getAgentsPollConfig   retry.PollConfigFactory
	stopAPIServerHandlers context.CancelFunc
	wf                    *connWorkerFactory
}

func (m *module) Run(ctx context.Context) error {
	wm := syncz.NewProtoWorkerManager[int64, *gapi.ReceptiveAgent](m.log, logz.AgentID, m.wf)
	defer wm.StopAllWorkers()
	defer m.stopAPIServerHandlers()

	_ = retry.PollWithBackoff(ctx, m.getAgentsPollConfig(), func(ctx context.Context) (error, retry.AttemptResult) {
		resp, err := gapi.GetReceptiveAgents(ctx, m.gitLabClient)
		if err != nil {
			if ctx.Err() == nil { // not a context error
				m.api.HandleProcessingError(ctx, m.log, "Failed to get receptive agents", err)
			}
			return nil, retry.Backoff
		}
		err = wm.ApplyConfiguration(agentsToWorkSources(resp.Agents))
		if err != nil {
			m.api.HandleProcessingError(ctx, m.log, "ApplyConfiguration error", err)
			// continue with existing configuration
		}
		return nil, retry.Continue
	})

	return nil
}

func (m *module) Name() string {
	return kas2agentk_tunnel.ModuleName
}

func agentsToWorkSources(agents []*gapi.ReceptiveAgent) []syncz.WorkSource[int64, *gapi.ReceptiveAgent] {
	cfgs := make([]syncz.WorkSource[int64, *gapi.ReceptiveAgent], 0, len(agents))

	for _, agent := range agents {
		cfgs = append(cfgs, syncz.WorkSource[int64, *gapi.ReceptiveAgent]{
			ID:            agent.Id,
			Configuration: agent,
		})
	}

	return cfgs
}
