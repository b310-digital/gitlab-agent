package agent

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net"

	"github.com/ash2k/stager"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/observability"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/prototool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/agentcfg"
)

type module struct {
	log                 *slog.Logger
	logLevel            *slog.LevelVar
	grpcLogLevel        *slog.LevelVar
	defaultGRPCLogLevel agentcfg.LogLevelEnum
	api                 modshared.API
	gatherer            prometheus.Gatherer
	registerer          prometheus.Registerer
	listener            func() (net.Listener, error)
	serverName          string
}

const (
	prometheusURLPath     = "/metrics"
	livenessProbeURLPath  = "/liveness"
	readinessProbeURLPath = "/readiness"
)

func (m *module) Run(ctx context.Context, cfg <-chan *agentcfg.AgentConfiguration) error {
	return stager.RunStages(ctx,
		func(stage stager.Stage) {
			// Listen for config changes and apply to logger
			stage.Go(func(ctx context.Context) error {
				done := ctx.Done()
				for {
					select {
					case <-done:
						return nil
					case config, ok := <-cfg:
						if !ok {
							return nil
						}
						err := m.setConfigurationLogging(config.Observability.Logging)
						if err != nil {
							m.log.Error("Failed to apply logging configuration", logz.Error(err))
							continue
						}
					}
				}
			})
			// Start metrics server
			stage.Go(func(ctx context.Context) error {
				lis, err := m.listener()
				if err != nil {
					return fmt.Errorf("observability listener failed to start: %w", err)
				}
				// Error is ignored because metricSrv.Run() closes the listener and
				// a second close always produces an error.
				defer lis.Close() //nolint:errcheck

				m.log.Info("Observability endpoint is up",
					logz.NetNetworkFromAddr(lis.Addr()),
					logz.NetAddressFromAddr(lis.Addr()),
				)

				metricSrv := observability.MetricServer{
					Log:                   m.log,
					API:                   m.api,
					Name:                  m.serverName,
					Listener:              lis,
					PrometheusURLPath:     prometheusURLPath,
					LivenessProbeURLPath:  livenessProbeURLPath,
					ReadinessProbeURLPath: readinessProbeURLPath,
					Gatherer:              m.gatherer,
					Registerer:            m.registerer,
					ProbeRegistry:         observability.NewProbeRegistry(),
				}

				return metricSrv.Run(ctx)
			})
		},
	)
}

func (m *module) DefaultAndValidateConfiguration(config *agentcfg.AgentConfiguration) error {
	prototool.NotNil(&config.Observability)
	prototool.NotNil(&config.Observability.Logging)
	err := m.defaultAndValidateLogging(config.Observability.Logging)
	if err != nil {
		return fmt.Errorf("logging: %w", err)
	}
	return nil
}

func (m *module) Name() string {
	return observability.ModuleName
}

func (m *module) defaultAndValidateLogging(logging *agentcfg.LoggingCF) error {
	if logging.GrpcLevel == nil {
		logging.GrpcLevel = &m.defaultGRPCLogLevel
	}
	return errors.Join(
		validateLogLevel(logging.Level),
		validateLogLevel(*logging.GrpcLevel),
	)
}

func (m *module) setConfigurationLogging(logging *agentcfg.LoggingCF) error {
	err := setLogLevel(m.logLevel, logging.Level)
	if err != nil {
		return err
	}

	return setLogLevel(m.grpcLogLevel, *logging.GrpcLevel) // not nil after defaulting
}

func setLogLevel(logLevel *slog.LevelVar, val agentcfg.LogLevelEnum) error {
	return logLevel.UnmarshalText([]byte(val.String()))
}

func validateLogLevel(val agentcfg.LogLevelEnum) error {
	var l slog.Level
	return l.UnmarshalText([]byte(val.String()))
}
