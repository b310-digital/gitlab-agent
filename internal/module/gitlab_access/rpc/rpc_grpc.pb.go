// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.5.1
// - protoc             v5.27.1
// source: internal/module/gitlab_access/rpc/rpc.proto

package rpc

import (
	context "context"

	grpctool "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.64.0 or later.
const _ = grpc.SupportPackageIsVersion9

const (
	GitlabAccess_MakeRequest_FullMethodName = "/gitlab.agent.gitlab_access.rpc.GitlabAccess/MakeRequest"
)

// GitlabAccessClient is the client API for GitlabAccess service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type GitlabAccessClient interface {
	MakeRequest(ctx context.Context, opts ...grpc.CallOption) (grpc.BidiStreamingClient[grpctool.HttpRequest, grpctool.HttpResponse], error)
}

type gitlabAccessClient struct {
	cc grpc.ClientConnInterface
}

func NewGitlabAccessClient(cc grpc.ClientConnInterface) GitlabAccessClient {
	return &gitlabAccessClient{cc}
}

func (c *gitlabAccessClient) MakeRequest(ctx context.Context, opts ...grpc.CallOption) (grpc.BidiStreamingClient[grpctool.HttpRequest, grpctool.HttpResponse], error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	stream, err := c.cc.NewStream(ctx, &GitlabAccess_ServiceDesc.Streams[0], GitlabAccess_MakeRequest_FullMethodName, cOpts...)
	if err != nil {
		return nil, err
	}
	x := &grpc.GenericClientStream[grpctool.HttpRequest, grpctool.HttpResponse]{ClientStream: stream}
	return x, nil
}

// This type alias is provided for backwards compatibility with existing code that references the prior non-generic stream type by name.
type GitlabAccess_MakeRequestClient = grpc.BidiStreamingClient[grpctool.HttpRequest, grpctool.HttpResponse]

// GitlabAccessServer is the server API for GitlabAccess service.
// All implementations must embed UnimplementedGitlabAccessServer
// for forward compatibility.
type GitlabAccessServer interface {
	MakeRequest(grpc.BidiStreamingServer[grpctool.HttpRequest, grpctool.HttpResponse]) error
	mustEmbedUnimplementedGitlabAccessServer()
}

// UnimplementedGitlabAccessServer must be embedded to have
// forward compatible implementations.
//
// NOTE: this should be embedded by value instead of pointer to avoid a nil
// pointer dereference when methods are called.
type UnimplementedGitlabAccessServer struct{}

func (UnimplementedGitlabAccessServer) MakeRequest(grpc.BidiStreamingServer[grpctool.HttpRequest, grpctool.HttpResponse]) error {
	return status.Errorf(codes.Unimplemented, "method MakeRequest not implemented")
}
func (UnimplementedGitlabAccessServer) mustEmbedUnimplementedGitlabAccessServer() {}
func (UnimplementedGitlabAccessServer) testEmbeddedByValue()                      {}

// UnsafeGitlabAccessServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to GitlabAccessServer will
// result in compilation errors.
type UnsafeGitlabAccessServer interface {
	mustEmbedUnimplementedGitlabAccessServer()
}

func RegisterGitlabAccessServer(s grpc.ServiceRegistrar, srv GitlabAccessServer) {
	// If the following call pancis, it indicates UnimplementedGitlabAccessServer was
	// embedded by pointer and is nil.  This will cause panics if an
	// unimplemented method is ever invoked, so we test this at initialization
	// time to prevent it from happening at runtime later due to I/O.
	if t, ok := srv.(interface{ testEmbeddedByValue() }); ok {
		t.testEmbeddedByValue()
	}
	s.RegisterService(&GitlabAccess_ServiceDesc, srv)
}

func _GitlabAccess_MakeRequest_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(GitlabAccessServer).MakeRequest(&grpc.GenericServerStream[grpctool.HttpRequest, grpctool.HttpResponse]{ServerStream: stream})
}

// This type alias is provided for backwards compatibility with existing code that references the prior non-generic stream type by name.
type GitlabAccess_MakeRequestServer = grpc.BidiStreamingServer[grpctool.HttpRequest, grpctool.HttpResponse]

// GitlabAccess_ServiceDesc is the grpc.ServiceDesc for GitlabAccess service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var GitlabAccess_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "gitlab.agent.gitlab_access.rpc.GitlabAccess",
	HandlerType: (*GitlabAccessServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "MakeRequest",
			Handler:       _GitlabAccess_MakeRequest_Handler,
			ServerStreams: true,
			ClientStreams: true,
		},
	},
	Metadata: "internal/module/gitlab_access/rpc/rpc.proto",
}
