package server

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
)

var (
	_ modserver.Factory = &Factory{}
)
