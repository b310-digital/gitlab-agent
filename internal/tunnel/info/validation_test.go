package info

import (
	"testing"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
)

func TestValidation_Invalid(t *testing.T) {
	tests := []testhelpers.InvalidTestcase{
		{
			ErrString: "validation error:\n - name: value length must be at least 1 bytes [string.min_bytes]",
			Invalid:   &Service{},
		},
		{
			ErrString: "validation error:\n - name: value length must be at least 1 bytes [string.min_bytes]",
			Invalid:   &Method{},
		},
	}
	testhelpers.AssertInvalid(t, tests)
}
