syntax = "proto3";

// If you make any changes make sure you run: make regenerate-proto

package gitlab.agent.gitlab.api;

option go_package = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab/api";

import "pkg/agentcfg/agentcfg.proto";
import "buf/validate/validate.proto";
import "pkg/entity/entity.proto";

// Configuration contains shared fields from agentcfg.CiAccessProjectCF and agentcfg.CiAccessGroupCF.
// It is used to parse response from the allowed_agents API endpoint.
// See https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/kubernetes_ci_access.md#apiv4joballowed_agents-api.
message Configuration {
  string default_namespace = 1 [json_name = "default_namespace"];
  agentcfg.CiAccessAsCF access_as = 2 [json_name = "access_as"];
}

message AllowedAgent {
  int64 id = 1 [json_name = "id"];
  ConfigProject config_project = 2 [json_name = "config_project", (buf.validate.field).required = true];
  Configuration configuration = 3 [json_name = "configuration"];
}

message ConfigProject {
  int64 id = 1 [json_name = "id"];
}

message Pipeline {
  int64 id = 1 [json_name = "id"];
}

message Project {
  int64 id = 1 [json_name = "id"];
  repeated Group groups = 2 [json_name = "groups"];
}

message Group {
  int64 id = 1 [json_name = "id"];
}

message Job {
  int64 id = 1 [json_name = "id"];
}

message User {
  int64 id = 1 [json_name = "id"];
  string username = 2 [json_name = "username", (buf.validate.field).string.min_bytes = 1];
}

message Environment {
  string slug = 1 [json_name = "slug", (buf.validate.field).string.min_bytes = 1];
  string tier = 2 [json_name = "tier", (buf.validate.field).string.min_bytes = 1];
}

message GetAgentInfoResponse {
  int64 project_id = 1 [json_name = "project_id", (buf.validate.field).int64.gt = 0];
  int64 agent_id = 2 [json_name = "agent_id", (buf.validate.field).int64.gt = 0];
  string agent_name = 3 [json_name = "agent_name", (buf.validate.field).string.min_bytes = 1];
  entity.GitalyInfo gitaly_info = 4 [json_name = "gitaly_info", (buf.validate.field).required = true];
  entity.GitalyRepository gitaly_repository = 5 [json_name = "gitaly_repository", (buf.validate.field).required = true];
  string default_branch = 6 [json_name = "default_branch", (buf.validate.field).string.min_bytes = 1];
}

message GetProjectInfoResponse {
  int64 project_id = 1 [json_name = "project_id", (buf.validate.field).int64.gt = 0];
  entity.GitalyInfo gitaly_info = 2 [json_name = "gitaly_info", (buf.validate.field).required = true];
  entity.GitalyRepository gitaly_repository = 3 [json_name = "gitaly_repository", (buf.validate.field).required = true];
  string default_branch = 4 [json_name = "default_branch", (buf.validate.field).string.min_bytes = 1];
}

message GetRepositoryInfoResponse {
  int64 project_id = 1 [json_name = "project_id", (buf.validate.field).int64.gt = 0];
  entity.GitalyInfo gitaly_info = 2 [json_name = "gitaly_info", (buf.validate.field).required = true];
  entity.GitalyRepository gitaly_repository = 3 [json_name = "gitaly_repository", (buf.validate.field).required = true];
  string default_branch = 4 [json_name = "default_branch", (buf.validate.field).string.min_bytes = 1];
}

message AllowedAgentsForJob {
  repeated AllowedAgent allowed_agents = 1 [json_name = "allowed_agents"];
  Job job = 2 [json_name = "job", (buf.validate.field).required = true];
  Pipeline pipeline = 3 [json_name = "pipeline", (buf.validate.field).required = true];
  Project project = 4 [json_name = "project", (buf.validate.field).required = true];
  User user = 5 [json_name = "user", (buf.validate.field).required = true];
  Environment environment = 6 [json_name = "environment"]; // optional
}

message AuthorizeProxyUserRequest {
  int64 agent_id = 1 [json_name = "agent_id", (buf.validate.field).int64.gt = 0];
  string access_type = 2 [json_name = "access_type", (buf.validate.field).string = {in: ["session_cookie", "personal_access_token"]}];
  string access_key = 3 [json_name = "access_key", (buf.validate.field).string.min_bytes = 1];
  string csrf_token = 4 [json_name = "csrf_token"];
}

message AuthorizeProxyUserResponse {
  AuthorizedAgentForUser agent = 1 [json_name = "agent", (buf.validate.field).required = true];
  User user = 2 [json_name = "user", (buf.validate.field).required = true];
  AccessAsProxyAuthorization access_as = 3 [json_name = "access_as", (buf.validate.field).required = true];
}

message AuthorizedAgentForUser {
  int64 id = 1 [json_name = "id"];
  ConfigProject config_project = 2 [json_name = "config_project", (buf.validate.field).required = true];
}

message AccessAsProxyAuthorization {
  oneof access_as {
    option (buf.validate.oneof).required = true;
    AccessAsAgentAuthorization agent = 1 [json_name = "agent"];
    AccessAsUserAuthorization user = 2 [json_name = "user"];
  }
}

message AccessAsAgentAuthorization {}

message AccessAsUserAuthorization {
  repeated ProjectAccessCF projects = 1 [json_name = "projects"];
  repeated GroupAccessCF groups = 2 [json_name = "groups"];
}

message ProjectAccessCF {
  int64 id = 1 [json_name = "id"];
  repeated string roles = 2 [json_name = "roles"];
}

message GroupAccessCF {
  int64 id = 1 [json_name = "id"];
  repeated string roles = 2 [json_name = "roles"];
}

message AgentConfigurationRequest {
  int64 agent_id = 1 [json_name = "agent_id", (buf.validate.field).int64.gt = 0];
  agentcfg.ConfigurationFile agent_config = 2 [json_name = "agent_config", (buf.validate.field).required = true];
}

message ReceptiveAgent {
  // Agent ID.
  int64 id = 1 [json_name = "id", (buf.validate.field).int64.gt = 0];
  // URL of the receptive agent.
  string url = 2 [json_name = "url", (buf.validate.field).string.uri = true];
  // base64-encoded EdDSA private key to generate a JWT signature for each request. Optional.
  string token = 3 [json_name = "token"];
  // Certificate authority certificate to validate the receptive agent's TLS certificate. Optional.
  string ca_cert = 4 [json_name = "ca_cert"];
  // Client certificate for mTLS. Optional.
  string client_cert = 5 [json_name = "client_cert"];
  // Client key for mTLS. Optional.
  string client_key = 6 [json_name = "client_key"];
  // Host name to use for TLS certificate validation instead of the one in the URL. Optional.
  string tls_host = 7 [json_name = "tls_host"];
}

message GetReceptiveAgentsResponse {
  repeated ReceptiveAgent agents = 1 [json_name = "agents"];
}
