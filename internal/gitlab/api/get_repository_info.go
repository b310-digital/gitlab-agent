package api

import (
	"context"
	"net/url"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab"
)

const (
	RepositoryInfoAPIPath             = "/api/v4/internal/kubernetes/repository_info"
	RepositoryInfoProjectIDQueryParam = "id"
)

func GetRepositoryInfo(ctx context.Context, client gitlab.ClientInterface, projectID string, opts ...gitlab.DoOption) (*api.ProjectInfo, error) {
	response := &GetRepositoryInfoResponse{}
	err := client.Do(ctx,
		joinOpts(opts,
			gitlab.WithPath(RepositoryInfoAPIPath),
			gitlab.WithQuery(url.Values{
				RepositoryInfoProjectIDQueryParam: []string{projectID},
			}),
			gitlab.WithResponseHandler(gitlab.ProtoJSONResponseHandler(response)),
			gitlab.WithJWT(true),
		)...,
	)
	if err != nil {
		return nil, err
	}
	return response.ToAPIProjectInfo(), nil
}
