package api

import (
	"context"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab"
)

const (
	ReceptiveAgentsAPIPath = "/api/v4/internal/kubernetes/receptive_agents"
)

func GetReceptiveAgents(ctx context.Context, client gitlab.ClientInterface, opts ...gitlab.DoOption) (*GetReceptiveAgentsResponse, error) {
	response := &GetReceptiveAgentsResponse{}
	err := client.Do(ctx,
		joinOpts(opts,
			gitlab.WithPath(ReceptiveAgentsAPIPath),
			gitlab.WithResponseHandler(gitlab.ProtoJSONResponseHandler(response)),
			gitlab.WithJWT(true),
		)...,
	)
	if err != nil {
		return nil, err
	}
	return response, nil
}
