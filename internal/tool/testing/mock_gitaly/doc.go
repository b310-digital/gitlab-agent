// Mocks for Gitaly.
package mock_gitaly

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitaly/vendored/gitalypb"
	"google.golang.org/grpc"
)

//go:generate mockgen.sh -source "../../../gitaly/vendored/gitalypb/commit_grpc.pb.go" -destination "gitaly.go" -package "mock_gitaly"

//go:generate mockgen.sh -source "doc.go" -destination "mock.go" -package "mock_gitaly"

// Workaround for https://github.com/uber-go/mock/issues/197.
type CommitService_GetTreeEntriesClient interface {
	grpc.ServerStreamingClient[gitalypb.GetTreeEntriesResponse]
}

// Workaround for https://github.com/uber-go/mock/issues/197.
type CommitService_TreeEntryClient interface {
	grpc.ServerStreamingClient[gitalypb.TreeEntryResponse]
}
