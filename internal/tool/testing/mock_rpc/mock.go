// Code generated by MockGen. DO NOT EDIT.
// Source: doc.go
//
// Generated by this command:
//
//	mockgen -typed -source doc.go -destination mock.go -package mock_rpc
//

// Package mock_rpc is a generated GoMock package.
package mock_rpc

import (
	context "context"
	reflect "reflect"

	rpc "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_configuration/rpc"
	gomock "go.uber.org/mock/gomock"
	metadata "google.golang.org/grpc/metadata"
)

// MockAgentConfiguration_GetConfigurationServer is a mock of AgentConfiguration_GetConfigurationServer interface.
type MockAgentConfiguration_GetConfigurationServer struct {
	ctrl     *gomock.Controller
	recorder *MockAgentConfiguration_GetConfigurationServerMockRecorder
}

// MockAgentConfiguration_GetConfigurationServerMockRecorder is the mock recorder for MockAgentConfiguration_GetConfigurationServer.
type MockAgentConfiguration_GetConfigurationServerMockRecorder struct {
	mock *MockAgentConfiguration_GetConfigurationServer
}

// NewMockAgentConfiguration_GetConfigurationServer creates a new mock instance.
func NewMockAgentConfiguration_GetConfigurationServer(ctrl *gomock.Controller) *MockAgentConfiguration_GetConfigurationServer {
	mock := &MockAgentConfiguration_GetConfigurationServer{ctrl: ctrl}
	mock.recorder = &MockAgentConfiguration_GetConfigurationServerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockAgentConfiguration_GetConfigurationServer) EXPECT() *MockAgentConfiguration_GetConfigurationServerMockRecorder {
	return m.recorder
}

// Context mocks base method.
func (m *MockAgentConfiguration_GetConfigurationServer) Context() context.Context {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Context")
	ret0, _ := ret[0].(context.Context)
	return ret0
}

// Context indicates an expected call of Context.
func (mr *MockAgentConfiguration_GetConfigurationServerMockRecorder) Context() *MockAgentConfiguration_GetConfigurationServerContextCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Context", reflect.TypeOf((*MockAgentConfiguration_GetConfigurationServer)(nil).Context))
	return &MockAgentConfiguration_GetConfigurationServerContextCall{Call: call}
}

// MockAgentConfiguration_GetConfigurationServerContextCall wrap *gomock.Call
type MockAgentConfiguration_GetConfigurationServerContextCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockAgentConfiguration_GetConfigurationServerContextCall) Return(arg0 context.Context) *MockAgentConfiguration_GetConfigurationServerContextCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockAgentConfiguration_GetConfigurationServerContextCall) Do(f func() context.Context) *MockAgentConfiguration_GetConfigurationServerContextCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockAgentConfiguration_GetConfigurationServerContextCall) DoAndReturn(f func() context.Context) *MockAgentConfiguration_GetConfigurationServerContextCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// RecvMsg mocks base method.
func (m_2 *MockAgentConfiguration_GetConfigurationServer) RecvMsg(m any) error {
	m_2.ctrl.T.Helper()
	ret := m_2.ctrl.Call(m_2, "RecvMsg", m)
	ret0, _ := ret[0].(error)
	return ret0
}

// RecvMsg indicates an expected call of RecvMsg.
func (mr *MockAgentConfiguration_GetConfigurationServerMockRecorder) RecvMsg(m any) *MockAgentConfiguration_GetConfigurationServerRecvMsgCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RecvMsg", reflect.TypeOf((*MockAgentConfiguration_GetConfigurationServer)(nil).RecvMsg), m)
	return &MockAgentConfiguration_GetConfigurationServerRecvMsgCall{Call: call}
}

// MockAgentConfiguration_GetConfigurationServerRecvMsgCall wrap *gomock.Call
type MockAgentConfiguration_GetConfigurationServerRecvMsgCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockAgentConfiguration_GetConfigurationServerRecvMsgCall) Return(arg0 error) *MockAgentConfiguration_GetConfigurationServerRecvMsgCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockAgentConfiguration_GetConfigurationServerRecvMsgCall) Do(f func(any) error) *MockAgentConfiguration_GetConfigurationServerRecvMsgCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockAgentConfiguration_GetConfigurationServerRecvMsgCall) DoAndReturn(f func(any) error) *MockAgentConfiguration_GetConfigurationServerRecvMsgCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// Send mocks base method.
func (m *MockAgentConfiguration_GetConfigurationServer) Send(arg0 *rpc.ConfigurationResponse) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Send", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// Send indicates an expected call of Send.
func (mr *MockAgentConfiguration_GetConfigurationServerMockRecorder) Send(arg0 any) *MockAgentConfiguration_GetConfigurationServerSendCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Send", reflect.TypeOf((*MockAgentConfiguration_GetConfigurationServer)(nil).Send), arg0)
	return &MockAgentConfiguration_GetConfigurationServerSendCall{Call: call}
}

// MockAgentConfiguration_GetConfigurationServerSendCall wrap *gomock.Call
type MockAgentConfiguration_GetConfigurationServerSendCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockAgentConfiguration_GetConfigurationServerSendCall) Return(arg0 error) *MockAgentConfiguration_GetConfigurationServerSendCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockAgentConfiguration_GetConfigurationServerSendCall) Do(f func(*rpc.ConfigurationResponse) error) *MockAgentConfiguration_GetConfigurationServerSendCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockAgentConfiguration_GetConfigurationServerSendCall) DoAndReturn(f func(*rpc.ConfigurationResponse) error) *MockAgentConfiguration_GetConfigurationServerSendCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SendHeader mocks base method.
func (m *MockAgentConfiguration_GetConfigurationServer) SendHeader(arg0 metadata.MD) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SendHeader", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// SendHeader indicates an expected call of SendHeader.
func (mr *MockAgentConfiguration_GetConfigurationServerMockRecorder) SendHeader(arg0 any) *MockAgentConfiguration_GetConfigurationServerSendHeaderCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SendHeader", reflect.TypeOf((*MockAgentConfiguration_GetConfigurationServer)(nil).SendHeader), arg0)
	return &MockAgentConfiguration_GetConfigurationServerSendHeaderCall{Call: call}
}

// MockAgentConfiguration_GetConfigurationServerSendHeaderCall wrap *gomock.Call
type MockAgentConfiguration_GetConfigurationServerSendHeaderCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockAgentConfiguration_GetConfigurationServerSendHeaderCall) Return(arg0 error) *MockAgentConfiguration_GetConfigurationServerSendHeaderCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockAgentConfiguration_GetConfigurationServerSendHeaderCall) Do(f func(metadata.MD) error) *MockAgentConfiguration_GetConfigurationServerSendHeaderCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockAgentConfiguration_GetConfigurationServerSendHeaderCall) DoAndReturn(f func(metadata.MD) error) *MockAgentConfiguration_GetConfigurationServerSendHeaderCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SendMsg mocks base method.
func (m_2 *MockAgentConfiguration_GetConfigurationServer) SendMsg(m any) error {
	m_2.ctrl.T.Helper()
	ret := m_2.ctrl.Call(m_2, "SendMsg", m)
	ret0, _ := ret[0].(error)
	return ret0
}

// SendMsg indicates an expected call of SendMsg.
func (mr *MockAgentConfiguration_GetConfigurationServerMockRecorder) SendMsg(m any) *MockAgentConfiguration_GetConfigurationServerSendMsgCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SendMsg", reflect.TypeOf((*MockAgentConfiguration_GetConfigurationServer)(nil).SendMsg), m)
	return &MockAgentConfiguration_GetConfigurationServerSendMsgCall{Call: call}
}

// MockAgentConfiguration_GetConfigurationServerSendMsgCall wrap *gomock.Call
type MockAgentConfiguration_GetConfigurationServerSendMsgCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockAgentConfiguration_GetConfigurationServerSendMsgCall) Return(arg0 error) *MockAgentConfiguration_GetConfigurationServerSendMsgCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockAgentConfiguration_GetConfigurationServerSendMsgCall) Do(f func(any) error) *MockAgentConfiguration_GetConfigurationServerSendMsgCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockAgentConfiguration_GetConfigurationServerSendMsgCall) DoAndReturn(f func(any) error) *MockAgentConfiguration_GetConfigurationServerSendMsgCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SetHeader mocks base method.
func (m *MockAgentConfiguration_GetConfigurationServer) SetHeader(arg0 metadata.MD) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SetHeader", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// SetHeader indicates an expected call of SetHeader.
func (mr *MockAgentConfiguration_GetConfigurationServerMockRecorder) SetHeader(arg0 any) *MockAgentConfiguration_GetConfigurationServerSetHeaderCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SetHeader", reflect.TypeOf((*MockAgentConfiguration_GetConfigurationServer)(nil).SetHeader), arg0)
	return &MockAgentConfiguration_GetConfigurationServerSetHeaderCall{Call: call}
}

// MockAgentConfiguration_GetConfigurationServerSetHeaderCall wrap *gomock.Call
type MockAgentConfiguration_GetConfigurationServerSetHeaderCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockAgentConfiguration_GetConfigurationServerSetHeaderCall) Return(arg0 error) *MockAgentConfiguration_GetConfigurationServerSetHeaderCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockAgentConfiguration_GetConfigurationServerSetHeaderCall) Do(f func(metadata.MD) error) *MockAgentConfiguration_GetConfigurationServerSetHeaderCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockAgentConfiguration_GetConfigurationServerSetHeaderCall) DoAndReturn(f func(metadata.MD) error) *MockAgentConfiguration_GetConfigurationServerSetHeaderCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SetTrailer mocks base method.
func (m *MockAgentConfiguration_GetConfigurationServer) SetTrailer(arg0 metadata.MD) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "SetTrailer", arg0)
}

// SetTrailer indicates an expected call of SetTrailer.
func (mr *MockAgentConfiguration_GetConfigurationServerMockRecorder) SetTrailer(arg0 any) *MockAgentConfiguration_GetConfigurationServerSetTrailerCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SetTrailer", reflect.TypeOf((*MockAgentConfiguration_GetConfigurationServer)(nil).SetTrailer), arg0)
	return &MockAgentConfiguration_GetConfigurationServerSetTrailerCall{Call: call}
}

// MockAgentConfiguration_GetConfigurationServerSetTrailerCall wrap *gomock.Call
type MockAgentConfiguration_GetConfigurationServerSetTrailerCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockAgentConfiguration_GetConfigurationServerSetTrailerCall) Return() *MockAgentConfiguration_GetConfigurationServerSetTrailerCall {
	c.Call = c.Call.Return()
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockAgentConfiguration_GetConfigurationServerSetTrailerCall) Do(f func(metadata.MD)) *MockAgentConfiguration_GetConfigurationServerSetTrailerCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockAgentConfiguration_GetConfigurationServerSetTrailerCall) DoAndReturn(f func(metadata.MD)) *MockAgentConfiguration_GetConfigurationServerSetTrailerCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// MockAgentConfiguration_GetConfigurationClient is a mock of AgentConfiguration_GetConfigurationClient interface.
type MockAgentConfiguration_GetConfigurationClient struct {
	ctrl     *gomock.Controller
	recorder *MockAgentConfiguration_GetConfigurationClientMockRecorder
}

// MockAgentConfiguration_GetConfigurationClientMockRecorder is the mock recorder for MockAgentConfiguration_GetConfigurationClient.
type MockAgentConfiguration_GetConfigurationClientMockRecorder struct {
	mock *MockAgentConfiguration_GetConfigurationClient
}

// NewMockAgentConfiguration_GetConfigurationClient creates a new mock instance.
func NewMockAgentConfiguration_GetConfigurationClient(ctrl *gomock.Controller) *MockAgentConfiguration_GetConfigurationClient {
	mock := &MockAgentConfiguration_GetConfigurationClient{ctrl: ctrl}
	mock.recorder = &MockAgentConfiguration_GetConfigurationClientMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockAgentConfiguration_GetConfigurationClient) EXPECT() *MockAgentConfiguration_GetConfigurationClientMockRecorder {
	return m.recorder
}

// CloseSend mocks base method.
func (m *MockAgentConfiguration_GetConfigurationClient) CloseSend() error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CloseSend")
	ret0, _ := ret[0].(error)
	return ret0
}

// CloseSend indicates an expected call of CloseSend.
func (mr *MockAgentConfiguration_GetConfigurationClientMockRecorder) CloseSend() *MockAgentConfiguration_GetConfigurationClientCloseSendCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CloseSend", reflect.TypeOf((*MockAgentConfiguration_GetConfigurationClient)(nil).CloseSend))
	return &MockAgentConfiguration_GetConfigurationClientCloseSendCall{Call: call}
}

// MockAgentConfiguration_GetConfigurationClientCloseSendCall wrap *gomock.Call
type MockAgentConfiguration_GetConfigurationClientCloseSendCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockAgentConfiguration_GetConfigurationClientCloseSendCall) Return(arg0 error) *MockAgentConfiguration_GetConfigurationClientCloseSendCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockAgentConfiguration_GetConfigurationClientCloseSendCall) Do(f func() error) *MockAgentConfiguration_GetConfigurationClientCloseSendCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockAgentConfiguration_GetConfigurationClientCloseSendCall) DoAndReturn(f func() error) *MockAgentConfiguration_GetConfigurationClientCloseSendCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// Context mocks base method.
func (m *MockAgentConfiguration_GetConfigurationClient) Context() context.Context {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Context")
	ret0, _ := ret[0].(context.Context)
	return ret0
}

// Context indicates an expected call of Context.
func (mr *MockAgentConfiguration_GetConfigurationClientMockRecorder) Context() *MockAgentConfiguration_GetConfigurationClientContextCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Context", reflect.TypeOf((*MockAgentConfiguration_GetConfigurationClient)(nil).Context))
	return &MockAgentConfiguration_GetConfigurationClientContextCall{Call: call}
}

// MockAgentConfiguration_GetConfigurationClientContextCall wrap *gomock.Call
type MockAgentConfiguration_GetConfigurationClientContextCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockAgentConfiguration_GetConfigurationClientContextCall) Return(arg0 context.Context) *MockAgentConfiguration_GetConfigurationClientContextCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockAgentConfiguration_GetConfigurationClientContextCall) Do(f func() context.Context) *MockAgentConfiguration_GetConfigurationClientContextCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockAgentConfiguration_GetConfigurationClientContextCall) DoAndReturn(f func() context.Context) *MockAgentConfiguration_GetConfigurationClientContextCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// Header mocks base method.
func (m *MockAgentConfiguration_GetConfigurationClient) Header() (metadata.MD, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Header")
	ret0, _ := ret[0].(metadata.MD)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Header indicates an expected call of Header.
func (mr *MockAgentConfiguration_GetConfigurationClientMockRecorder) Header() *MockAgentConfiguration_GetConfigurationClientHeaderCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Header", reflect.TypeOf((*MockAgentConfiguration_GetConfigurationClient)(nil).Header))
	return &MockAgentConfiguration_GetConfigurationClientHeaderCall{Call: call}
}

// MockAgentConfiguration_GetConfigurationClientHeaderCall wrap *gomock.Call
type MockAgentConfiguration_GetConfigurationClientHeaderCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockAgentConfiguration_GetConfigurationClientHeaderCall) Return(arg0 metadata.MD, arg1 error) *MockAgentConfiguration_GetConfigurationClientHeaderCall {
	c.Call = c.Call.Return(arg0, arg1)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockAgentConfiguration_GetConfigurationClientHeaderCall) Do(f func() (metadata.MD, error)) *MockAgentConfiguration_GetConfigurationClientHeaderCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockAgentConfiguration_GetConfigurationClientHeaderCall) DoAndReturn(f func() (metadata.MD, error)) *MockAgentConfiguration_GetConfigurationClientHeaderCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// Recv mocks base method.
func (m *MockAgentConfiguration_GetConfigurationClient) Recv() (*rpc.ConfigurationResponse, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Recv")
	ret0, _ := ret[0].(*rpc.ConfigurationResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Recv indicates an expected call of Recv.
func (mr *MockAgentConfiguration_GetConfigurationClientMockRecorder) Recv() *MockAgentConfiguration_GetConfigurationClientRecvCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Recv", reflect.TypeOf((*MockAgentConfiguration_GetConfigurationClient)(nil).Recv))
	return &MockAgentConfiguration_GetConfigurationClientRecvCall{Call: call}
}

// MockAgentConfiguration_GetConfigurationClientRecvCall wrap *gomock.Call
type MockAgentConfiguration_GetConfigurationClientRecvCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockAgentConfiguration_GetConfigurationClientRecvCall) Return(arg0 *rpc.ConfigurationResponse, arg1 error) *MockAgentConfiguration_GetConfigurationClientRecvCall {
	c.Call = c.Call.Return(arg0, arg1)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockAgentConfiguration_GetConfigurationClientRecvCall) Do(f func() (*rpc.ConfigurationResponse, error)) *MockAgentConfiguration_GetConfigurationClientRecvCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockAgentConfiguration_GetConfigurationClientRecvCall) DoAndReturn(f func() (*rpc.ConfigurationResponse, error)) *MockAgentConfiguration_GetConfigurationClientRecvCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// RecvMsg mocks base method.
func (m_2 *MockAgentConfiguration_GetConfigurationClient) RecvMsg(m any) error {
	m_2.ctrl.T.Helper()
	ret := m_2.ctrl.Call(m_2, "RecvMsg", m)
	ret0, _ := ret[0].(error)
	return ret0
}

// RecvMsg indicates an expected call of RecvMsg.
func (mr *MockAgentConfiguration_GetConfigurationClientMockRecorder) RecvMsg(m any) *MockAgentConfiguration_GetConfigurationClientRecvMsgCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RecvMsg", reflect.TypeOf((*MockAgentConfiguration_GetConfigurationClient)(nil).RecvMsg), m)
	return &MockAgentConfiguration_GetConfigurationClientRecvMsgCall{Call: call}
}

// MockAgentConfiguration_GetConfigurationClientRecvMsgCall wrap *gomock.Call
type MockAgentConfiguration_GetConfigurationClientRecvMsgCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockAgentConfiguration_GetConfigurationClientRecvMsgCall) Return(arg0 error) *MockAgentConfiguration_GetConfigurationClientRecvMsgCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockAgentConfiguration_GetConfigurationClientRecvMsgCall) Do(f func(any) error) *MockAgentConfiguration_GetConfigurationClientRecvMsgCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockAgentConfiguration_GetConfigurationClientRecvMsgCall) DoAndReturn(f func(any) error) *MockAgentConfiguration_GetConfigurationClientRecvMsgCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// SendMsg mocks base method.
func (m_2 *MockAgentConfiguration_GetConfigurationClient) SendMsg(m any) error {
	m_2.ctrl.T.Helper()
	ret := m_2.ctrl.Call(m_2, "SendMsg", m)
	ret0, _ := ret[0].(error)
	return ret0
}

// SendMsg indicates an expected call of SendMsg.
func (mr *MockAgentConfiguration_GetConfigurationClientMockRecorder) SendMsg(m any) *MockAgentConfiguration_GetConfigurationClientSendMsgCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SendMsg", reflect.TypeOf((*MockAgentConfiguration_GetConfigurationClient)(nil).SendMsg), m)
	return &MockAgentConfiguration_GetConfigurationClientSendMsgCall{Call: call}
}

// MockAgentConfiguration_GetConfigurationClientSendMsgCall wrap *gomock.Call
type MockAgentConfiguration_GetConfigurationClientSendMsgCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockAgentConfiguration_GetConfigurationClientSendMsgCall) Return(arg0 error) *MockAgentConfiguration_GetConfigurationClientSendMsgCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockAgentConfiguration_GetConfigurationClientSendMsgCall) Do(f func(any) error) *MockAgentConfiguration_GetConfigurationClientSendMsgCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockAgentConfiguration_GetConfigurationClientSendMsgCall) DoAndReturn(f func(any) error) *MockAgentConfiguration_GetConfigurationClientSendMsgCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// Trailer mocks base method.
func (m *MockAgentConfiguration_GetConfigurationClient) Trailer() metadata.MD {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Trailer")
	ret0, _ := ret[0].(metadata.MD)
	return ret0
}

// Trailer indicates an expected call of Trailer.
func (mr *MockAgentConfiguration_GetConfigurationClientMockRecorder) Trailer() *MockAgentConfiguration_GetConfigurationClientTrailerCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Trailer", reflect.TypeOf((*MockAgentConfiguration_GetConfigurationClient)(nil).Trailer))
	return &MockAgentConfiguration_GetConfigurationClientTrailerCall{Call: call}
}

// MockAgentConfiguration_GetConfigurationClientTrailerCall wrap *gomock.Call
type MockAgentConfiguration_GetConfigurationClientTrailerCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockAgentConfiguration_GetConfigurationClientTrailerCall) Return(arg0 metadata.MD) *MockAgentConfiguration_GetConfigurationClientTrailerCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockAgentConfiguration_GetConfigurationClientTrailerCall) Do(f func() metadata.MD) *MockAgentConfiguration_GetConfigurationClientTrailerCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockAgentConfiguration_GetConfigurationClientTrailerCall) DoAndReturn(f func() metadata.MD) *MockAgentConfiguration_GetConfigurationClientTrailerCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}
