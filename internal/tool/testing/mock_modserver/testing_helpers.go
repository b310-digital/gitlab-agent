package mock_modserver

import (
	"context"
	"testing"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/entity"
)

func IncomingAgentCtx(t *testing.T, rpcAPI *MockAgentRPCAPI) context.Context {
	rpcAPI.EXPECT().
		AgentToken().
		Return(testhelpers.AgentkToken).
		AnyTimes()
	rpcAPI.EXPECT().
		Log().
		Return(testhelpers.NewLogger(t)).
		AnyTimes()
	ctx, cancel := context.WithCancel(context.Background())
	t.Cleanup(cancel)
	ctx = modserver.InjectAgentRPCAPI(ctx, rpcAPI)

	return ctx
}

func AgentMeta() *entity.AgentMeta {
	return &entity.AgentMeta{
		Version:      "v17.1.0",
		GitRef:       "0123456789abcdef0123456789abcdef00000000",
		PodNamespace: "ns1",
		PodName:      "n1",
		KubernetesVersion: &entity.KubernetesVersion{
			Major:    "1",
			Minor:    "30",
			Platform: "linux/amd64",
		},
	}
}
