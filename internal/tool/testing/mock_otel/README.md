OpenTelemetry uses embedded interfaces with unexported methods. gomock does not support that, see
https://github.com/uber-go/mock/issues/23 and https://github.com/uber-go/mock/issues/64.
We patch the generated mocks manually here.

- Use `generate_otel_fixed.sh` to generate `otel_fixed.go` from the gomock-generated `otel.go`. This uses the existing patch (if it can be applied).
- Make necessary changes, like adding embedded interfaces to new mocks.
- Run `generate_patch.sh` to regenerate the `otel.go.patch` patch file.
- Run `apply_patch.sh` to apply the patch to `otel.go`.
- Remove or rename `otel_fixed.go` and check if things work.
