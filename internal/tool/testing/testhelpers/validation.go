package testhelpers

import (
	"testing"

	"github.com/bufbuild/protovalidate-go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"google.golang.org/protobuf/proto"
)

type InvalidTestcase struct {
	ErrString string
	Invalid   proto.Message
}

type ValidTestcase struct {
	Name  string
	Valid proto.Message
}

func AssertInvalid(t *testing.T, tests []InvalidTestcase) {
	v, err := protovalidate.New()
	require.NoError(t, err)

	for _, tc := range tests {
		t.Run(tc.ErrString, func(t *testing.T) {
			err := v.Validate(tc.Invalid)
			assert.EqualError(t, err, tc.ErrString)
		})
	}
}

func AssertValid(t *testing.T, tests []ValidTestcase) {
	v, err := protovalidate.New()
	require.NoError(t, err)

	for _, tc := range tests {
		t.Run(tc.Name, func(t *testing.T) {
			assert.NoError(t, v.Validate(tc.Valid))
		})
	}
}
