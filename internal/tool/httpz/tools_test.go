package httpz

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestMergeURLPathAndQuery(t *testing.T) {
	tests := []struct {
		baseURL   string
		extraPath string
		query     url.Values
		expected  string
	}{
		{
			baseURL:  "/ok",
			expected: "/ok",
		},
		{
			baseURL:   "/ok",
			extraPath: "/abc",
			expected:  "/ok/abc",
		},
		{
			baseURL:   "/ok",
			extraPath: "/abc/",
			expected:  "/ok/abc/",
		},
		{
			baseURL:   "/ok/",
			extraPath: "/abc/",
			expected:  "/ok/abc/",
		},
		{
			baseURL:   "/ok",
			extraPath: "abc",
			expected:  "/ok/abc",
		},
		{
			baseURL:   "/ok/",
			extraPath: "abc",
			expected:  "/ok/abc",
		},
		{
			baseURL:   "/ok?a=b",
			extraPath: "/abc",
			expected:  "/ok/abc?a=b",
		},
		{
			baseURL:   "/ok",
			extraPath: "/abc",
			query: map[string][]string{
				"c": {"d", "e"},
			},
			expected: "/ok/abc?c=d&c=e",
		},
		{
			baseURL:   "/ok?a=b",
			extraPath: "/abc",
			query: map[string][]string{
				"c": {"d", "e"},
			},
			expected: "/ok/abc?a=b&c=d&c=e",
		},
		{
			baseURL:   "/ok?a=b",
			extraPath: "/abc",
			query: map[string][]string{
				"a": {"d", "e"},
			},
			expected: "/ok/abc?a=d&a=e", // override values, don't merge
		},
	}

	for _, tc := range tests {
		t.Run(tc.expected, func(t *testing.T) {
			base, err := url.Parse(tc.baseURL)
			require.NoError(t, err)
			actual := MergeURLPathAndQuery(base, tc.extraPath, tc.query)
			assert.Equal(t, tc.expected, actual)
		})
		t.Run("example.com"+tc.expected, func(t *testing.T) {
			base, err := url.Parse("example.com" + tc.baseURL)
			require.NoError(t, err)
			actual := MergeURLPathAndQuery(base, tc.extraPath, tc.query)
			assert.Equal(t, "example.com"+tc.expected, actual)
		})
	}
}
