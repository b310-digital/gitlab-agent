package prototool

import "google.golang.org/protobuf/proto"

// Validator is a validator of protobuf messages.
type Validator interface {
	Validate(proto.Message) error
}

type NopValidator struct{}

func (v NopValidator) Validate(proto.Message) error {
	return nil
}
