package ioz

import (
	"encoding/base64"
	"fmt"
	"io"
	"os"
)

// DiscardData discards up to 8KiB of data from r.
// Function can be used to drain HTTP client response body.
// See https://pkg.go.dev/net/http#Response.
func DiscardData(r io.Reader) error {
	_, err := io.Copy(io.Discard, io.LimitReader(r, 8*1024))
	if err != nil {
		return fmt.Errorf("failed to read data: %w", err)
	}
	return nil
}

func LoadBase64Secret(filename string) ([]byte, error) {
	encodedAuthSecret, err := os.ReadFile(filename) //nolint: gosec
	if err != nil {
		return nil, fmt.Errorf("read file: %w", err)
	}
	decodedAuthSecret := make([]byte, len(encodedAuthSecret))

	n, err := base64.StdEncoding.Decode(decodedAuthSecret, encodedAuthSecret)
	if err != nil {
		return nil, fmt.Errorf("decoding: %w", err)
	}
	return decodedAuthSecret[:n], nil
}
