package fieldz

import "time"

const (
	AgentIDFieldName         = "agent_id"
	LastConnectedAtFieldName = "recently_connected_at"
	SecondsAgoFieldName      = "seconds_ago"
)

// Field represents a single key value pair for logging and error tracking purposes.
type Field struct {
	Key   string
	Value any
}

func NewField(key string, value any) Field {
	return Field{Key: key, Value: value}
}

func ToMap(fields []Field) map[string]any {
	m := make(map[string]any, len(fields))
	for _, field := range fields {
		m[field.Key] = field.Value
	}
	return m
}

func AgentID(agentID int64) Field {
	return Field{Key: AgentIDFieldName, Value: agentID}
}

func LastConnectedAt(timestamp time.Time) Field {
	return Field{Key: LastConnectedAtFieldName, Value: timestamp.UTC()}
}

func SecondsAgo(duration time.Duration) Field {
	return Field{Key: SecondsAgoFieldName, Value: duration}
}
