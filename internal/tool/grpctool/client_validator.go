package grpctool

import (
	"context"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/prototool"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"
)

// UnaryClientValidatingInterceptor is a unary client interceptor that performs response validation.
func UnaryClientValidatingInterceptor(v prototool.Validator) grpc.UnaryClientInterceptor {
	return func(parentCtx context.Context, method string, req, reply any, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		err := invoker(parentCtx, method, req, reply, cc, opts...)
		if err != nil {
			return err
		}
		return maybeValidate(v, reply)
	}
}

// StreamClientValidatingInterceptor is a stream client interceptor that performs response stream validation.
func StreamClientValidatingInterceptor(v prototool.Validator) grpc.StreamClientInterceptor {
	return func(parentCtx context.Context, desc *grpc.StreamDesc, cc *grpc.ClientConn, method string, streamer grpc.Streamer, opts ...grpc.CallOption) (grpc.ClientStream, error) {
		stream, err := streamer(parentCtx, desc, cc, method, opts...)
		if err != nil {
			return nil, err
		}
		return recvWrapper{
			ClientStream: stream,
			validator:    v,
		}, nil
	}
}

type recvWrapper struct {
	grpc.ClientStream
	validator prototool.Validator
}

func (w recvWrapper) RecvMsg(m any) error {
	if err := w.ClientStream.RecvMsg(m); err != nil {
		return err
	}
	return maybeValidate(w.validator, m)
}

func maybeValidate(v prototool.Validator, msg any) error {
	if m, ok := msg.(proto.Message); ok {
		if err := v.Validate(m); err != nil {
			return status.Errorf(codes.InvalidArgument, "invalid server response: %v", err)
		}
	}
	return nil
}
