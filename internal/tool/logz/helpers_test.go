package logz

import (
	"bytes"
	"log/slog"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStrings(t *testing.T) {
	b := &bytes.Buffer{}
	l := slog.New(slog.NewTextHandler(b, &slog.HandlerOptions{
		ReplaceAttr: func(groups []string, attr slog.Attr) slog.Attr {
			if attr.Key == slog.TimeKey {
				return slog.Attr{}
			}
			return attr
		},
	}))

	l.Info("test1", Strings("k1", []string{"1"}))
	l.Info("test2", Strings("k2", []string{"1", "2"}))

	expected := `level=INFO msg=test1 k1=[1]
level=INFO msg=test2 k2="[1 2]"
`

	assert.Equal(t, expected, b.String())
}
